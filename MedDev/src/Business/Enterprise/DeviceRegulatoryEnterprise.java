/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class DeviceRegulatoryEnterprise extends Enterprise {

    public DeviceRegulatoryEnterprise(String name) {
        super(name, EnterpriseType.DeviceRegulatory);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
}
