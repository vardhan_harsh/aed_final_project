/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class EnterpriseDirectory {

    private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }

    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type) {
        Enterprise enterprise = null;

        if (null != type) switch (type) {
            case Hospital:
                enterprise = new HospitalEnterprise(name);
                enterpriseList.add(enterprise);
                break;
            case Manufacturer:
                enterprise = new ManufacturerEnterprise(name);
                enterpriseList.add(enterprise);
                break;
            case Distributor:
                enterprise = new DistributorEnterprise(name);
                enterpriseList.add(enterprise);
                break;
            case DeviceRegulatory:
                enterprise = new DeviceRegulatoryEnterprise(name);
                enterpriseList.add(enterprise);
                break;
            default:
                break;
        }

        return enterprise;
    }

    public void removeEnterprise(Enterprise e) {
        enterpriseList.remove(e);
    }
}
