/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Network.Network;
import Business.Network.State;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem system;
    private OrganizationDirectory organizationDirectory;
    private ArrayList<Network> networkList;

    public static EcoSystem getInstance() {
        if (system == null) {
            system = new EcoSystem();
        }
        return system;
    }

    private EcoSystem() {
        super(null);
        organizationDirectory = new OrganizationDirectory();
        networkList = new ArrayList<>();
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }

    public Network createCountryNetwork(String countryName) {
        Network network = null;
        boolean flag = false;
        if (system.getNetworkList().isEmpty()) {
            Network n = new Network();
            n.setCountryName(countryName);
            networkList.add(n);
            network = n;
        }

        for (Network net : system.getNetworkList()) {
            if (net.getCountryName().equals(countryName)) {
                network = net;
                flag = true;
            }

        }
        if (flag == false) {
            Network n = new Network();
            n.setCountryName(countryName);
            networkList.add(n);
            network = n;
        }
        return network;
    }

    public void removeCountryNetwork(Network n) {
        networkList.remove(n);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public String getCountryName(State state) {
        for (Network n : networkList) {
            for (State s : n.getStateList()) {
                if (s.equals(state)) {
                    return n.getCountryName();
                }
            }
        }
        return null;
    }
}
