/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public abstract class Role {

    public enum RoleType {
        Admin("Admin"),
        InventoryMgmt("Inventory Managment"),
        HTMEngineer("HTM Engineer"),
        Supplier("Supplier Role"),
        DepartmentAdmin("Department Admin"),
        MaintenanceManagerAdmin("Maintenance Manager Admin"),
        InventoryAdmin("Inventory Admin"),
        DeviceManufacturer("Device Manufacturer"),
        DeviceRegulatoryEmployeeRole("Device Regulatory Employee");

        private String value;

        private RoleType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public abstract JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, State state, EcoSystem system);

    @Override
    public String toString() {
        return this.getClass().getName().replace("Business.Role.", "");
    }
}
