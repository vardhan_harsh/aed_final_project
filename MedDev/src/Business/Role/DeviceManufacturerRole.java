/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.DeviceProductionOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.DeviceManufacturer.DeviceManufacturerWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class DeviceManufacturerRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, State state, EcoSystem system) {
        return new DeviceManufacturerWorkAreaJPanel(userProcessContainer, account, (DeviceProductionOrganization) organization, enterprise, state, system );
    }
}
