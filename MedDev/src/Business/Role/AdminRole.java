/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Admin.AdminWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class AdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, State state, EcoSystem system) {
        return new AdminWorkAreaJPanel(userProcessContainer, enterprise, system);
    }
}
