/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.InventoryOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.Inventory.InventoryManagmentWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class InventoryAdminRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, State state, EcoSystem system) {
        return new InventoryManagmentWorkAreaJPanel(userProcessContainer, account, (InventoryOrganization) organization, enterprise, state, system);
    }

}
