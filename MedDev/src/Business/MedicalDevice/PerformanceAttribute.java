/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Harsh
 */
public class PerformanceAttribute {

    private double temperature;
    private double frequency;
    private double powerSpectralDensity;
    private int deviceRegulatoryClass;
    private double chargeTime;
    private int load;
    private double health;
    private Date sensorTimestamp;

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public Date getSensorTimestamp() {
        return sensorTimestamp;
    }

    public void setSensorTimestamp(Date sensorTimestamp) {
        this.sensorTimestamp = sensorTimestamp;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public double getPowerSpectralDensity() {
        return powerSpectralDensity;
    }

    public void setPowerSpectralDensity(double powerSpectralDensity) {
        this.powerSpectralDensity = powerSpectralDensity;
    }

    public int getDeviceRegulatoryClass() {
        return deviceRegulatoryClass;
    }

    public void setDeviceRegulatoryClass(int deviceRegulatoryClass) {
        this.deviceRegulatoryClass = deviceRegulatoryClass;
    }

    public double getChargeTime() {
        return chargeTime;
    }

    public void setChargeTime(double chargeTime) {
        this.chargeTime = chargeTime;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

//    @Override
//    public String toString() {
//        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' hh:mm:ss a");
//        return ft.format(sensorTimestamp);
//    }
    
}
