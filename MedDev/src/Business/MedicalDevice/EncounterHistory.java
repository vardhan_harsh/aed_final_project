/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class EncounterHistory {

    private ArrayList<Encounter> encounterList;

    public EncounterHistory() {
        encounterList = new ArrayList<>();
    }

    public Encounter createAndAddEncounter() {
        Encounter ec = new Encounter();
        encounterList.add(ec);
        return ec;
    }

    public void deleteEnc(Encounter enc) {
        encounterList.remove(enc);
    }

    public ArrayList<Encounter> getEncounterList() {
        return encounterList;
    }

    public void setEncounterList(ArrayList<Encounter> encounterList) {
        this.encounterList = encounterList;
    }
    
    
}
