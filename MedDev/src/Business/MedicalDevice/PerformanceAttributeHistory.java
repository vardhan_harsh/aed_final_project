/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class PerformanceAttributeHistory {

    private ArrayList<PerformanceAttribute> performanceHistoryAttributeList;

    public PerformanceAttributeHistory() {
        performanceHistoryAttributeList = new ArrayList<>();
    }

    public void addPerformance(PerformanceAttribute pa) {
        performanceHistoryAttributeList.add(pa);
    }

    public ArrayList<PerformanceAttribute> getPerformanceHistoryAttributeList() {
        return performanceHistoryAttributeList;
    }

    public void setPerformanceHistoryAttributeList(ArrayList<PerformanceAttribute> performanceHistoryAttributeList) {
        this.performanceHistoryAttributeList = performanceHistoryAttributeList;
    }

    public PerformanceAttribute createAndAddPerformanceAttribute() {
        PerformanceAttribute pa = new PerformanceAttribute();
        performanceHistoryAttributeList.add(pa);
        return pa;
    }
}
