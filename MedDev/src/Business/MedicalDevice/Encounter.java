/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Harsh
 */
public class Encounter {

    private boolean failure;
    private boolean maintenance;
    private Date encounterTimestamp;

    public Encounter() {
    }

    public boolean isFailure() {
        return failure;
    }

    public void setFailure(boolean failure) {
        this.failure = failure;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }

    public Date getEncounterTimestamp() {
        return encounterTimestamp;
    }

    public void setEncounterTimestamp(Date encounterTimestamp) {
        this.encounterTimestamp = encounterTimestamp;
    }

    @Override
    public String toString() {
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' hh:mm:ss a");
        return ft.format(encounterTimestamp);
    }
}
