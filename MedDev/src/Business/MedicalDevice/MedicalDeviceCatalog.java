/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class MedicalDeviceCatalog {

    private ArrayList<MedicalDevice> medicalDeviceList;

    public MedicalDeviceCatalog() {
        medicalDeviceList = new ArrayList<>();
    }

    public ArrayList<MedicalDevice> getMedicalDeviceList() {
        return medicalDeviceList;
    }

    public void setMedicalDeviceList(ArrayList<MedicalDevice> medicalDeviceList) {
        this.medicalDeviceList = medicalDeviceList;
    }

    public MedicalDevice addMedicalDevice() {
        MedicalDevice md = new MedicalDevice();
        medicalDeviceList.add(md);
        return md;
    }

    public void scrapDevice(MedicalDevice md) {
        medicalDeviceList.remove(md);
    }

    public boolean isDeviceCodePresent(String deviceCode) {
        for (MedicalDevice md : medicalDeviceList) {
            if (md.getDeviceCode().equalsIgnoreCase(deviceCode)) {
                return true;
            }
        }
        return false;
    }

    public void addMedicalDevice(MedicalDevice md) {
        medicalDeviceList.add(md);
    }
}
