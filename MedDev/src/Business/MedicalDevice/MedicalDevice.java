/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalDevice;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Harsh
 */
public class MedicalDevice {

    private int rfid;
    private String deviceCode;
    private String deviceName;
    private double price;
    private Date warrantyDate;
    private Date manufacturingDate;
    private int quantityAvailable; //Quantity in Inventory
    private int quantityNeeded;//Quantity needed by hospitals
    private boolean warrantry;
    private String location;
    private ArrayList<String> department;
    private boolean testedOk;
    private EncounterHistory encounterHistory;
    private PerformanceAttributeHistory performanceHistory;
    private PerformanceAttribute performanceAttribute;
    private static int count = 0;

    public MedicalDevice() {
        count++;
        rfid = count;
        encounterHistory = new EncounterHistory();
        performanceHistory = new PerformanceAttributeHistory();
        performanceAttribute = new PerformanceAttribute();
        department = new ArrayList<>();
    }

    public MedicalDevice(boolean isNew) {
        encounterHistory = new EncounterHistory();
        performanceHistory = new PerformanceAttributeHistory();
        performanceAttribute = new PerformanceAttribute();
        department = new ArrayList<>();
    }

    public int getRfid() {
        return rfid;
    }

    public void setRfid(int rfid) {
        this.rfid = rfid;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(Date warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public int getQuantityNeeded() {
        return quantityNeeded;
    }

    public void setQuantityNeeded(int quantityNeeded) {
        this.quantityNeeded = quantityNeeded;
    }

    public boolean isWarrantry() {
        return warrantry;
    }

    public void setWarrantry(boolean warrantry) {
        this.warrantry = warrantry;
    }

    public ArrayList<String> getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department.add(department);
    }

    public EncounterHistory getEncounterHistory() {
        return encounterHistory;
    }

    public void setEncounterHistory(EncounterHistory encounterHistory) {
        this.encounterHistory = encounterHistory;
    }

    public PerformanceAttributeHistory getPerformanceHistory() {
        return performanceHistory;
    }

    public void setPerformanceHistory(PerformanceAttributeHistory performanceHistory) {
        this.performanceHistory = performanceHistory;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public PerformanceAttribute getPerformanceAttribute() {
        return performanceAttribute;
    }

    public void setPerformanceAttribute(PerformanceAttribute performanceAttribute) {
        this.performanceAttribute = performanceAttribute;
    }

    public boolean isTestedOk() {
        return testedOk;
    }

    public void setTestedOk(boolean testedOk) {
        this.testedOk = testedOk;
    }

    @Override
    public String toString() {
        return deviceCode.toUpperCase();
    }

}
