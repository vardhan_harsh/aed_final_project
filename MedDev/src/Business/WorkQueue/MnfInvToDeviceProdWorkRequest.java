/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.MedicalDevice.MedicalDevice;

/**
 *
 * @author Harsh
 */
public class MnfInvToDeviceProdWorkRequest extends WorkRequest {

    private String result;
    private MedicalDevice medicalDevice;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public MedicalDevice getMedicalDevice() {
        return medicalDevice;
    }

    public void setMedicalDevice(MedicalDevice medicalDevice) {
        this.medicalDevice = medicalDevice;
    }

}
