/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.MedicalDevice.MedicalDeviceCatalog;
import Business.Role.HTMEngineerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class HTMOrganization extends Organization {

    private MedicalDeviceCatalog medicalDeviceCatalog;

    public HTMOrganization() {
        super(Type.HTM.getValue());
        medicalDeviceCatalog = new MedicalDeviceCatalog();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new HTMEngineerRole());
        return roles;
    }

    public MedicalDeviceCatalog getMedicalDeviceCatalog() {
        return medicalDeviceCatalog;
    }

    public void setMedicalDeviceCatalog(MedicalDeviceCatalog medicalDeviceCatalog) {
        this.medicalDeviceCatalog = medicalDeviceCatalog;
    }

}
