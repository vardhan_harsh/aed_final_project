/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.MedicalDevice.MedicalDeviceCatalog;
import Business.Role.MaintenanceManagerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class MaintenanceDepartmentOrganization extends Organization {

    private MedicalDeviceCatalog deviceCatalog;

    public MaintenanceDepartmentOrganization() {
        super(Type.MaintenanceDepartment.getValue());
        deviceCatalog = new MedicalDeviceCatalog();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new MaintenanceManagerRole());
        return roles;
    }

    public MedicalDeviceCatalog getDeviceCatalog() {
        return deviceCatalog;
    }

    public void setDeviceCatalog(MedicalDeviceCatalog deviceCatalog) {
        this.deviceCatalog = deviceCatalog;
    }

}
