/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DeviceRegulatoryEmployeeRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class DeviceRegulatoryEmployeeOrganization extends Organization {

    public DeviceRegulatoryEmployeeOrganization() {
        super(Type.DeviceRegulatoryEmployee.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new DeviceRegulatoryEmployeeRole());
        return roles;
    }

}
