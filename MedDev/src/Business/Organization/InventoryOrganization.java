/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.MedicalDevice.MedicalDeviceCatalog;
import Business.Role.InventoryAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class InventoryOrganization extends Organization {

    private MedicalDeviceCatalog medicalDeviceCatalog;

    public InventoryOrganization() {
        super(Type.Inventory.getValue());
        medicalDeviceCatalog = new MedicalDeviceCatalog();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new InventoryAdminRole());
        return roles;
    }

    public MedicalDeviceCatalog getMedicalDeviceCatalog() {
        return medicalDeviceCatalog;
    }

    public void setMedicalDeviceCatalog(MedicalDeviceCatalog medicalDeviceCatalog) {
        this.medicalDeviceCatalog = medicalDeviceCatalog;
    }

}
