/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createOrganization(Organization.Type type) {
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Inventory.getValue())) {
            organization = new InventoryOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Organization.Type.HTM.getValue())) {
            organization = new HTMOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Organization.Type.MaintenanceDepartment.getValue())) {
            organization = new MaintenanceDepartmentOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Organization.Type.DeviceRegulatoryEmployee.getValue())) {
            organization = new DeviceRegulatoryEmployeeOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Organization.Type.DeviceProduction.getValue())) {
            organization = new DeviceProductionOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Organization.Type.Supplier.getValue())) {
            organization = new SupplierOrganization();
            organizationList.add(organization);
        }
        return organization;
    }

    public void deleteOrganization(Organization organization) {
        organizationList.remove(organization);
    }
}
