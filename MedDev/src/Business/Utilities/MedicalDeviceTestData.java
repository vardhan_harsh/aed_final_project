/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

/**
 *
 * @author Harsh
 */
public class MedicalDeviceTestData {

    double minTemperature;
    double maxTemperature;
    double minFrequency;
    double maxFrequency;
    double minPowerSpectralDensity;
    double maxPowerSpectralDensity;
    int deviceRegulatoryClass;
    double minChargeTime;
    double maxChargeTime;
    int minLoad;
    int maxLoad;

    public MedicalDeviceTestData(int deviceRegulatoryClass, String countryName) {
        if (!countryName.equalsIgnoreCase("INDIA")) {
            switch (deviceRegulatoryClass) {
                case 1:
                    minTemperature = -10;
                    maxTemperature = 40;
                    minFrequency = 0.20;
                    maxFrequency = 50;
                    minPowerSpectralDensity = 50;
                    maxPowerSpectralDensity = 100;
                    minChargeTime = 5;
                    maxChargeTime = 7;
                    minLoad = 0;
                    maxLoad = 30;
                    break;
                case 2:
                    minTemperature = -15;
                    maxTemperature = 45;
                    minFrequency = 0.10;
                    maxFrequency = 60;
                    minPowerSpectralDensity = 60;
                    maxPowerSpectralDensity = 110;
                    minChargeTime = 6;
                    maxChargeTime = 8;
                    minLoad = 0;
                    maxLoad = 70;
                    break;
                case 3:
                    minTemperature = -20;
                    maxTemperature = 50;
                    minFrequency = 0.01;
                    maxFrequency = 70;
                    minPowerSpectralDensity = 40;
                    maxPowerSpectralDensity = 130;
                    minChargeTime = 3;
                    maxChargeTime = 9;
                    minLoad = 0;
                    maxLoad = 100;
                    break;
                default:
                    break;
            }
        } else {
            switch (deviceRegulatoryClass) {
                case 1:
                    minTemperature = -12;
                    maxTemperature = 30;
                    minFrequency = 0.50;
                    maxFrequency = 60;
                    minPowerSpectralDensity = 40;
                    maxPowerSpectralDensity = 90;
                    minChargeTime = 5;
                    maxChargeTime = 7;
                    minLoad = 0;
                    maxLoad = 40;
                    break;
                case 2:
                    minTemperature = -15;
                    maxTemperature = 40;
                    minFrequency = 0.40;
                    maxFrequency = 70;
                    minPowerSpectralDensity = 50;
                    maxPowerSpectralDensity = 100;
                    minChargeTime = 6;
                    maxChargeTime = 8;
                    minLoad = 0;
                    maxLoad = 80;
                    break;
                case 3:
                    minTemperature = -18;
                    maxTemperature = 50;
                    minFrequency = 0.01;
                    maxFrequency = 70;
                    minPowerSpectralDensity = 20;
                    maxPowerSpectralDensity = 120;
                    minChargeTime = 3;
                    maxChargeTime = 9;
                    minLoad = 0;
                    maxLoad = 100;
                    break;
                default:
                    break;
            }
        }
    }

    public double getMinTemperature() {
        return minTemperature;
    }

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public double getMinFrequency() {
        return minFrequency;
    }

    public double getMaxFrequency() {
        return maxFrequency;
    }

    public double getMinPowerSpectralDensity() {
        return minPowerSpectralDensity;
    }

    public double getMaxPowerSpectralDensity() {
        return maxPowerSpectralDensity;
    }

    public int getDeviceRegulatoryClass() {
        return deviceRegulatoryClass;
    }

    public double getMinChargeTime() {
        return minChargeTime;
    }

    public double getMaxChargeTime() {
        return maxChargeTime;
    }

    public int getMinLoad() {
        return minLoad;
    }

    public int getMaxLoad() {
        return maxLoad;
    }

}
