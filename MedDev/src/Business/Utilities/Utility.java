/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Utilities;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author Harsh
 */
public class Utility {

    public static boolean textOnly(JTextField f, String errormsg) {
        if (!(Pattern.matches("^[a-zA-Z\\s]+$", f.getText()))) {
            return failedMessage(f, errormsg);
        } else {
            return true; // validation successful
        }
    }

    public static boolean validateField(JTextField f, String errormsg) {
        if (f.getText().trim().equals("")) {
            return failedMessage(f, errormsg);
        } else {
            return true; // validation successful
        }
    }

    public static boolean validatPasswordeField(JPasswordField f, String errormsg) {
        if (String.valueOf(f.getPassword()).equals("")) {
            return failedMessage(f, errormsg);
        } else {
            return true; // validation successful
        }
    }

    public static boolean TryParseInt(JTextField f, String errMsg) {
        try {
            int i = Integer.parseInt(f.getText().trim());
            if (i > 0) {
                return true;
            }
        } catch (NumberFormatException ex) {
        }
        return failedMessage(f, errMsg);
    }

    public static boolean TryParseLong(JTextField f, String errMsg) {
        try {
            long i = Long.parseLong(f.getText().trim());
            if (i > 0) {
                return true;
            }
        } catch (NumberFormatException ex) {
        }
        return failedMessage(f, errMsg);
    }

    public static boolean TryParseDouble(JTextField f, String errMsg) {
        try {
            double i = Double.parseDouble(f.getText());
            if (i > 0) {
                return true;
            }
        } catch (NumberFormatException ex) {
        }
        return failedMessage(f, errMsg);
    }

    public static boolean failedMessage(JTextField f, String errormsg) {
        JOptionPane.showMessageDialog(null, errormsg, "Error", JOptionPane.ERROR_MESSAGE); // give user feedback
        f.requestFocus(); // set focus on field, so user can change
        return false; // return false, as validation has failed
    }

    public static long getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static boolean compareTimePart(Date greaterDate, Date smallerDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(greaterDate);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(smallerDate);
        if (cal.get(Calendar.HOUR) > cal2.get(Calendar.HOUR)) {
            return true;
        } else if (cal.get(Calendar.HOUR) == cal2.get(Calendar.HOUR)) {
            if (cal.get(Calendar.MINUTE) > cal2.get(Calendar.MINUTE)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static String formatedStringDate(Date dt) {
        SimpleDateFormat formatDepartDate = new SimpleDateFormat("MM/dd/yyyy");
        return formatDepartDate.format(dt);
    }

    public static String formatedStringTime(Date time) {
        SimpleDateFormat formatDepartTime = new SimpleDateFormat("HH:mm");
        return formatDepartTime.format(time);
    }

    public static boolean isValidSSN(JTextField ssn, String errormsg) {
        if (ssn.getText().length() != 6) {
            return failedMessage(ssn, errormsg);
        } else {
            return true;
        }
    }

    public static void populateChart(JPanel jpanel, PieDataset data, StringBuilder strbl) {
        JFreeChart chart = createChart(data, strbl);
        jpanel.setLayout(new java.awt.BorderLayout());
        ChartPanel cp = new ChartPanel(chart) {

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(900, 550);
            }
        };
        cp.setMouseWheelEnabled(true);
        jpanel.add(cp, BorderLayout.CENTER);
        jpanel.validate();
    }

    private static JFreeChart createChart(PieDataset dataset, StringBuilder strbl) {

        JFreeChart chart = ChartFactory.createPieChart(
                "", // chart title
                dataset, // data
                true, // include legend
                true,
                false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        for (int i = 0; i < dataset.getItemCount(); i++) {
            if (strbl.indexOf(dataset.getKey(i).toString()) == -1) {
                plot.setSectionPaint(dataset.getKey(i), Color.GREEN);
            } else {
                plot.setSectionPaint(dataset.getKey(i), Color.RED);
            }
        }
        plot.setSectionOutlinesVisible(false);
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(true);
        plot.setLabelGap(0.02);
        PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator(
                "{0}: {1} ({2})", new DecimalFormat("0.00"), new DecimalFormat("0.00%"));
        plot.setLabelGenerator(gen);
        return chart;
    }

    public static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order) {

        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1,
                    Map.Entry<String, Double> o2) {
                if (order) {
                    return (int) (o1.getValue() - o2.getValue());
                } else {
                    return (int) (o2.getValue() - o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static double randDouble(int min, int max) {

        Random rand = new Random();
        double randomNum = (max - min) * rand.nextDouble() + min + rand.nextDouble();
        return randomNum;
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;

    }
}
