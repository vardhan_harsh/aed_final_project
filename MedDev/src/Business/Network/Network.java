/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import java.util.ArrayList;

/**
 *
 * @author Harsh
 */
public class Network {
     private String countryName;
    private ArrayList<State> stateList;

    public Network() {
        stateList = new ArrayList<>();
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<State> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<State> stateList) {
        this.stateList = stateList;
    }

    public State addState(String statename) {
        State stateNetwork = null;
        boolean flag = false;
        if (stateList.isEmpty()) {
            State state = new State();
            state.setStatename(statename);
            stateList.add(state);
            // added
            stateNetwork = state;
            return state;

        } else {
            for (State sn : stateList) {
                if (sn.getStatename().equals(statename)) {
                    stateNetwork = sn;
               // } else {
                    // say statenetwork found
                    flag = true;
                }

            }
            if (flag == false) {
                State state = new State();
                state.setStatename(statename);
                stateList.add(state);
                return state;
            }
        }
        return stateNetwork;

    }

    @Override
    public String toString() {
        return countryName;
    }
    
}
