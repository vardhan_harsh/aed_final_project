/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.DeviceRegulatoryEmployee;

import Business.EcoSystem;
import Business.MedicalDevice.MedicalDevice;
import Business.MedicalDevice.PerformanceAttribute;
import Business.Network.State;
import Business.Organization.DeviceRegulatoryEmployeeOrganization;
import Business.UserAccount.UserAccount;
import Business.Utilities.MedicalDeviceTestData;
import Business.WorkQueue.DeviceProdToFDAWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.SystemColor;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Harsh
 */
public class ProcessDeviceTestingWorkRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ProcessDeviceTestingWorkRequestJPanel
     */
    JPanel userProcessContainer;
    DeviceRegulatoryEmployeeOrganization fdaEmpOrganization;
    UserAccount userAccount;
    MedicalDeviceTestData mdtd;
    StringBuilder invalidAttr;
    PerformanceAttribute selectedPA;
    EcoSystem system;
    State state;

    public ProcessDeviceTestingWorkRequestJPanel(JPanel userProcessContainer, UserAccount account, DeviceRegulatoryEmployeeOrganization fdaEmpOrganization, State state, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.fdaEmpOrganization = fdaEmpOrganization;
        this.system = system;
        this.state = state;
        populateManufacturerToFDARequest();
        jTabbedPane1.setIconAt(0, new ImageIcon(getClass().getResource("/Images/icons8_Minimum_Value_20px.png")));
        jTabbedPane1.setIconAt(1, new ImageIcon(getClass().getResource("/Images/icons8_Temperature_20px.png")));
        jTabbedPane1.setIconAt(2, new ImageIcon(getClass().getResource("/Images/icons8_Frequency_20px.png")));
        jTabbedPane1.setIconAt(3, new ImageIcon(getClass().getResource("/Images/icons8_Audio_Wave_20px.png")));
        jTabbedPane1.setIconAt(4, new ImageIcon(getClass().getResource("/Images/icons8_Weight_20px_1.png")));
        jTabbedPane1.addChangeListener((ChangeEvent ce) -> {
            if (jTabbedPane1.getSelectedComponent().equals(panelFreq)) {
                if (selectedPA != null) {
                    populateFreq(selectedPA);
                }
            } else if (jTabbedPane1.getSelectedComponent().equals(panelLoad)) {
                if (selectedPA != null) {
                    populateLoad(selectedPA);
                }
            } else if (jTabbedPane1.getSelectedComponent().equals(panelPSD)) {
                if (selectedPA != null) {
                    populatePSD(selectedPA);
                }
            } else if (jTabbedPane1.getSelectedComponent().equals(panelTemp)) {
                if (selectedPA != null) {
                    populateTemp(selectedPA);
                }
            }
        });
    }

    private void populateIdealValues() {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Ideal Values",
                "Attributes",
                "Values",
                createIdealDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
        panelIdeal.setLayout(new java.awt.BorderLayout());
        panelIdeal.removeAll();
        panelIdeal.add(chartPanel, BorderLayout.CENTER);
        panelIdeal.validate();
    }

    private CategoryDataset createIdealDataset() {
        final String temprature = "Temprature";
        final String Frequency = "Frequency";
        final String powerSpectralDensity = "Power Spectral Density";
        final String load = "Load";
        final String health = "Health";
        final String maximum = "Maximum";
        final String minimum = "Minimum";
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();

        dataset.addValue(mdtd.getMinTemperature(), minimum, temprature);
        dataset.addValue(mdtd.getMaxTemperature(), maximum, temprature);

        dataset.addValue(mdtd.getMinFrequency(), minimum, Frequency);
        dataset.addValue(mdtd.getMaxFrequency(), maximum, Frequency);

        dataset.addValue(mdtd.getMaxPowerSpectralDensity(), maximum, powerSpectralDensity);
        dataset.addValue(mdtd.getMinPowerSpectralDensity(), minimum, powerSpectralDensity);

        dataset.addValue(mdtd.getMaxLoad(), maximum, load);
        dataset.addValue(mdtd.getMinLoad(), minimum, load);

        dataset.addValue(100, maximum, health);
        dataset.addValue(0.0, minimum, health);

        return dataset;
    }

    private void populateTemp(PerformanceAttribute pa) {
        CategoryDataset dataset = createTempDataset(pa);
        JFreeChart barChart = ChartFactory.createBarChart(
                "Device Temprature",
                "",
                "Values",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        CategoryPlot cplot = (CategoryPlot) barChart.getPlot();
        cplot.setBackgroundPaint(SystemColor.inactiveCaption);//change background color
        //set  bar chart color
        ((BarRenderer) cplot.getRenderer()).setBarPainter(new StandardBarPainter());
        BarRenderer r = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        r.setMaximumBarWidth(.10);
        if (pa.getTemperature() >= mdtd.getMinTemperature() && pa.getTemperature() <= mdtd.getMaxTemperature()) {
            r.setSeriesPaint(0, Color.GREEN);
        } else {
            r.setSeriesPaint(0, Color.RED);
        }
        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 400));
        panelTemp.setLayout(new java.awt.BorderLayout());
        panelTemp.removeAll();
        panelTemp.add(chartPanel, BorderLayout.CENTER);
        panelTemp.validate();
    }

    private CategoryDataset createTempDataset(PerformanceAttribute pa) {
        final String temprature = "Temprature";
        final String value = "Value " + pa.getTemperature();
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();
        dataset.addValue(pa.getTemperature(), value, temprature);
        return dataset;
    }

    private void populateFreq(PerformanceAttribute pa) {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Device Frequency",
                "",
                "Values",
                createFreqDataset(pa),
                PlotOrientation.VERTICAL,
                true, true, false);
        CategoryPlot cplot = (CategoryPlot) barChart.getPlot();
        cplot.setBackgroundPaint(SystemColor.inactiveCaption);//change background color
        //set  bar chart color
        ((BarRenderer) cplot.getRenderer()).setBarPainter(new StandardBarPainter());
        BarRenderer r = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        r.setMaximumBarWidth(.10);
        if (pa.getFrequency() >= mdtd.getMinFrequency() && pa.getTemperature() <= mdtd.getMaxFrequency()) {
            r.setSeriesPaint(0, Color.GREEN);
        } else {
            r.setSeriesPaint(0, Color.RED);
        }
        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 400));
        panelFreq.setLayout(new java.awt.BorderLayout());
        panelFreq.removeAll();
        panelFreq.add(chartPanel, BorderLayout.CENTER);
        panelFreq.validate();
    }

    private CategoryDataset createFreqDataset(PerformanceAttribute pa) {
        final String Frequency = "Frequency";
        final String value = "Value " + pa.getFrequency();
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();
        dataset.addValue(pa.getFrequency(), value, Frequency);
        return dataset;
    }

    private void populatePSD(PerformanceAttribute pa) {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Device Power Spectral Density",
                "",
                "Values",
                createPSDDataset(pa),
                PlotOrientation.VERTICAL,
                true, true, false);
        CategoryPlot cplot = (CategoryPlot) barChart.getPlot();
        cplot.setBackgroundPaint(SystemColor.inactiveCaption);//change background color
        //set  bar chart color
        ((BarRenderer) cplot.getRenderer()).setBarPainter(new StandardBarPainter());
        BarRenderer r = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        r.setMaximumBarWidth(.10);
        if (pa.getPowerSpectralDensity() >= mdtd.getMinPowerSpectralDensity() && pa.getPowerSpectralDensity() <= mdtd.getMaxPowerSpectralDensity()) {
            r.setSeriesPaint(0, Color.GREEN);
        } else {
            r.setSeriesPaint(0, Color.RED);
        }
        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 400));
        panelPSD.setLayout(new java.awt.BorderLayout());
        panelPSD.removeAll();
        panelPSD.add(chartPanel, BorderLayout.CENTER);
        panelPSD.validate();
    }

    private CategoryDataset createPSDDataset(PerformanceAttribute pa) {
        final String PowerSpectralDensity = "Power Spectral Density";
        final String value = "Value " + pa.getPowerSpectralDensity();
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();
        dataset.addValue(pa.getPowerSpectralDensity(), value, PowerSpectralDensity);
        return dataset;
    }

    private void populateLoad(PerformanceAttribute pa) {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Device Load",
                "",
                "Values",
                createLoadDataset(pa),
                PlotOrientation.VERTICAL,
                true, true, false);
        CategoryPlot cplot = (CategoryPlot) barChart.getPlot();
        cplot.setBackgroundPaint(SystemColor.inactiveCaption);//change background color
        //set  bar chart color
        ((BarRenderer) cplot.getRenderer()).setBarPainter(new StandardBarPainter());
        BarRenderer r = (BarRenderer) barChart.getCategoryPlot().getRenderer();
        r.setMaximumBarWidth(.10);
        if (pa.getLoad() >= mdtd.getMinLoad() && pa.getLoad() <= mdtd.getMaxLoad()) {
            r.setSeriesPaint(0, Color.GREEN);
        } else {
            r.setSeriesPaint(0, Color.RED);
        }
        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 400));
        panelLoad.setLayout(new java.awt.BorderLayout());
        panelLoad.removeAll();
        panelLoad.add(chartPanel, BorderLayout.CENTER);
        panelLoad.validate();
    }

    private CategoryDataset createLoadDataset(PerformanceAttribute pa) {
        final String load = "Load";
        final String value = "Value " + pa.getLoad();
        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();
        dataset.addValue(pa.getLoad(), value, load);
        return dataset;
    }

    public void populateManufacturerToFDARequest() {

        DefaultTableModel dtm = (DefaultTableModel) tblManufacturerToFDA.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblManufacturerToFDA.setRowSorter(sorter);

        DeviceProdToFDAWorkRequest request = null;
        dtm.setRowCount(0);
        for (WorkRequest request2 : fdaEmpOrganization.getWorkQueue().getWorkRequestList()) {
            if (request2 instanceof DeviceProdToFDAWorkRequest) {
                request = (DeviceProdToFDAWorkRequest) request2;
                Object[] row = new Object[7];
                row[0] = userAccount;
                row[1] = request.getMedicalDevice().getRfid();
                row[2] = request.getMedicalDevice().getDeviceName();
                row[3] = request.getMedicalDevice().isTestedOk();
                row[4] = request.getStatus();
                String result = request.getResult();
                row[6] = result == null ? "Waiting" : result;
                row[5] = request;
                dtm.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tblManufacturerToFDA = new javax.swing.JTable();
        btnTestDevice = new javax.swing.JButton();
        btnViewTestResult = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelIdeal = new javax.swing.JPanel();
        panelTemp = new javax.swing.JPanel();
        panelFreq = new javax.swing.JPanel();
        panelPSD = new javax.swing.JPanel();
        panelLoad = new javax.swing.JPanel();
        btnBack2 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Medical Device Testing", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        tblManufacturerToFDA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Device RfID", "Device Name", "Tested", "Status", "Message", "Result"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblManufacturerToFDA);

        btnTestDevice.setBackground(new java.awt.Color(51, 122, 183));
        btnTestDevice.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnTestDevice.setForeground(new java.awt.Color(255, 255, 255));
        btnTestDevice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Submit_Progress_20px.png"))); // NOI18N
        btnTestDevice.setText("Test Device");
        btnTestDevice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestDeviceActionPerformed(evt);
            }
        });

        btnViewTestResult.setBackground(new java.awt.Color(51, 122, 183));
        btnViewTestResult.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnViewTestResult.setForeground(new java.awt.Color(255, 255, 255));
        btnViewTestResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_View_20px.png"))); // NOI18N
        btnViewTestResult.setText("View Test Results");
        btnViewTestResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewTestResultActionPerformed(evt);
            }
        });

        jTabbedPane1.setBackground(new java.awt.Color(51, 122, 183));
        jTabbedPane1.setForeground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout panelIdealLayout = new javax.swing.GroupLayout(panelIdeal);
        panelIdeal.setLayout(panelIdealLayout);
        panelIdealLayout.setHorizontalGroup(
            panelIdealLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 696, Short.MAX_VALUE)
        );
        panelIdealLayout.setVerticalGroup(
            panelIdealLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Device Ideal Values  ", panelIdeal);

        javax.swing.GroupLayout panelTempLayout = new javax.swing.GroupLayout(panelTemp);
        panelTemp.setLayout(panelTempLayout);
        panelTempLayout.setHorizontalGroup(
            panelTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 696, Short.MAX_VALUE)
        );
        panelTempLayout.setVerticalGroup(
            panelTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Temperature  ", panelTemp);

        javax.swing.GroupLayout panelFreqLayout = new javax.swing.GroupLayout(panelFreq);
        panelFreq.setLayout(panelFreqLayout);
        panelFreqLayout.setHorizontalGroup(
            panelFreqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 696, Short.MAX_VALUE)
        );
        panelFreqLayout.setVerticalGroup(
            panelFreqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Frequency  ", panelFreq);

        javax.swing.GroupLayout panelPSDLayout = new javax.swing.GroupLayout(panelPSD);
        panelPSD.setLayout(panelPSDLayout);
        panelPSDLayout.setHorizontalGroup(
            panelPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 696, Short.MAX_VALUE)
        );
        panelPSDLayout.setVerticalGroup(
            panelPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Power Spectral Density  ", panelPSD);

        javax.swing.GroupLayout panelLoadLayout = new javax.swing.GroupLayout(panelLoad);
        panelLoad.setLayout(panelLoadLayout);
        panelLoadLayout.setHorizontalGroup(
            panelLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 696, Short.MAX_VALUE)
        );
        panelLoadLayout.setVerticalGroup(
            panelLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Load  ", panelLoad);

        btnBack2.setBackground(new java.awt.Color(51, 122, 183));
        btnBack2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack2.setForeground(new java.awt.Color(255, 255, 255));
        btnBack2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack2.setText("Back");
        btnBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnBack2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnViewTestResult, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnTestDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTestDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnViewTestResult, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnTestDeviceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestDeviceActionPerformed
        JFrame frame = new JFrame("Test");
        int selectedRow = tblManufacturerToFDA.getSelectedRow();

        if (selectedRow >= 0) {
            DeviceProdToFDAWorkRequest request = (DeviceProdToFDAWorkRequest) tblManufacturerToFDA.getValueAt(selectedRow, 5);
            if (!request.getStatus().equalsIgnoreCase("Completed")) {
                new SwingWorker<Void, String>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        btnViewTestResult.setEnabled(false);
                        btnTestDevice.setEnabled(false);
                        URL resource = this.getClass().getClassLoader()
                                .getResource("Images/ajax-loader.gif");
                        ImageIcon loading = new ImageIcon(resource);
                        frame.add(new JPanel().add(new JLabel("", loading, JLabel.CENTER)));
                        frame.setDefaultCloseOperation(0);
                        frame.setUndecorated(true);
                        frame.setBounds(500, 300, 250, 50);
                        frame.setVisible(true);
                        Thread.sleep(3000);
                        return null;
                    }

                    @Override
                    protected void done() {
                        frame.setVisible(false);
                        btnViewTestResult.setEnabled(true);
                        btnTestDevice.setEnabled(true);

                        request.setStatus("Processing");
                        deviceTest(request);

                    }
                }.execute();
            } else {
                JOptionPane.showMessageDialog(null, "Device is already tested.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select a request to process.");
        }
    }//GEN-LAST:event_btnTestDeviceActionPerformed

    private void btnViewTestResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewTestResultActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblManufacturerToFDA.getSelectedRow();

        if (selectedRow >= 0) {
            DeviceProdToFDAWorkRequest request = (DeviceProdToFDAWorkRequest) tblManufacturerToFDA.getValueAt(selectedRow, 5);
            if (request.getResult() != null && !request.getResult().isEmpty()) {
                MedicalDevice md = request.getMedicalDevice();
                selectedPA = md.getPerformanceAttribute();
                int deviceRegulatoryClass = selectedPA.getDeviceRegulatoryClass();
                mdtd = new MedicalDeviceTestData(deviceRegulatoryClass, system.getCountryName(state));
                populateIdealValues();
                populateTemp(selectedPA);
                populateFreq(selectedPA);
                populatePSD(selectedPA);
                populateLoad(selectedPA);
            } else {
                JOptionPane.showMessageDialog(null, "Please perform the device test to view results.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select a request to process.");
        }
    }//GEN-LAST:event_btnViewTestResultActionPerformed

    private void btnBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack2ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack2ActionPerformed

    private void deviceTest(DeviceProdToFDAWorkRequest request) {

        MedicalDevice md = request.getMedicalDevice();
        PerformanceAttribute pa = md.getPerformanceAttribute();
        int deviceRegulatoryClass = pa.getDeviceRegulatoryClass();
        mdtd = new MedicalDeviceTestData(deviceRegulatoryClass, system.getCountryName(state));
        invalidAttr = new StringBuilder();
        boolean isValid = true;
        switch (deviceRegulatoryClass) {
            case 1:
                isValid = comparePerformance(pa, mdtd, isValid);
                break;
            case 2:
                isValid = comparePerformance(pa, mdtd, isValid);
                break;
            case 3:
                isValid = comparePerformance(pa, mdtd, isValid);
                break;
            default:
                break;
        }

        if (isValid) {
            request.setResult("Passed");
            request.setStatus("Completed");
            request.setMessage("Device Test Passed.");
            md.setTestedOk(true);
            populateManufacturerToFDARequest();
            JOptionPane.showMessageDialog(null, "Device Tested Successfully. PASSED", "Success", JOptionPane.INFORMATION_MESSAGE);
        } else {
            request.setResult("Failed");
            request.setStatus("Completed");
            request.setMessage("Device Test Failed. Please check marked attribute - " + invalidAttr.toString());
            populateManufacturerToFDARequest();
            JOptionPane.showMessageDialog(null, "Device Tested Successfully. FAILED", "Failed", JOptionPane.WARNING_MESSAGE);
        }
    }

    private boolean comparePerformance(PerformanceAttribute pa, MedicalDeviceTestData mdtd, boolean isValid) {
//        if (!(pa.getChargeTime() >= mdtd.getMinChargeTime() && pa.getChargeTime() <= mdtd.getMaxChargeTime())) {
//            invalidAttr.append("ChargeTime.");
//            isValid = false;
//        }
        if (!(pa.getFrequency() >= mdtd.getMinFrequency() && pa.getFrequency() <= mdtd.getMaxFrequency())) {
            invalidAttr.append("Frequency.");
            isValid = false;
        }
        if (!(pa.getLoad() >= mdtd.getMinLoad() && pa.getLoad() <= mdtd.getMaxLoad())) {
            invalidAttr.append("Load.");
            isValid = false;
        }
        if (!(pa.getPowerSpectralDensity() >= mdtd.getMinChargeTime() && pa.getPowerSpectralDensity() <= mdtd.getMaxPowerSpectralDensity())) {
            invalidAttr.append("PowerSpectralDensity.");
            isValid = false;
        }
        if (!(pa.getTemperature() >= mdtd.getMinTemperature() && pa.getTemperature() <= mdtd.getMaxTemperature())) {
            invalidAttr.append("Temperature.");
            isValid = false;
        }
        return isValid;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack2;
    private javax.swing.JButton btnTestDevice;
    private javax.swing.JButton btnViewTestResult;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel panelFreq;
    private javax.swing.JPanel panelIdeal;
    private javax.swing.JPanel panelLoad;
    private javax.swing.JPanel panelPSD;
    private javax.swing.JPanel panelTemp;
    private javax.swing.JTable tblManufacturerToFDA;
    // End of variables declaration//GEN-END:variables
}
