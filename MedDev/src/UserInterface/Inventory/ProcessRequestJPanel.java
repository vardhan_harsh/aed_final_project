/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Inventory;

import Business.Enterprise.Enterprise;
import Business.MedicalDevice.MedicalDevice;
import Business.MedicalDevice.MedicalDeviceCatalog;
import Business.Network.State;
import Business.Organization.InventoryOrganization;
import Business.Organization.MaintenanceDepartmentOrganization;
import Business.Organization.Organization;
import Business.Organization.SupplierOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.DepartmentToHospInventoryWorkReuest;
import Business.WorkQueue.InventoryToInventoryWorkRequest;
import Business.WorkQueue.InventoryToSupplierWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Harsh
 */
public class ProcessRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ProcessRequestJPanel
     */
    JPanel userProcessContainer;
    InventoryOrganization inventoryOrganization;
    UserAccount userAccount;
    Enterprise enterprise;
    State state;
    MedicalDeviceCatalog rqtSenderDeviceCatalog;
    MedicalDeviceCatalog enterpriseDeviceCatalog;

    public ProcessRequestJPanel(JPanel userProcessContainer, UserAccount account, InventoryOrganization inventoryOrganization, Enterprise enterprise, State state) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.inventoryOrganization = inventoryOrganization;
        this.enterprise = enterprise;
        this.state = state;
        populateTable();
        populateWorkRequestTable();
    }

    public void populateTable() {
        DefaultTableModel dtm = (DefaultTableModel) tblManfDeviceCatalog.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblManfDeviceCatalog.setRowSorter(sorter);
        dtm.setRowCount(0);
        if (!enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Manufacturer)) {
            for (MedicalDevice md : inventoryOrganization.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                Object row[] = new Object[5];
                row[0] = md.getRfid();
                row[1] = md.getDeviceCode();
                row[2] = md;
                row[3] = md.isTestedOk();
                row[4] = md.getQuantityAvailable();
                dtm.addRow(row);
            }
        } else {
            for (MedicalDevice md : enterprise.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                Object row[] = new Object[5];
                row[0] = md.getRfid();
                row[1] = md.getDeviceCode();
                row[2] = md;
                row[3] = md.isTestedOk();
                row[4] = md.getQuantityAvailable();
                dtm.addRow(row);
            }
        }
    }

    public void populateWorkRequestTable() {

        DefaultTableModel dtm = (DefaultTableModel) tblRequest.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblRequest.setRowSorter(sorter);

        InventoryToSupplierWorkRequest requestIToISP = null;
        DepartmentToHospInventoryWorkReuest requestHTMIToH = null;
        InventoryToInventoryWorkRequest requestIToI = null;
        dtm.setRowCount(0);
        switch (enterprise.getEnterpriseType()) {
            case Distributor:
                for (WorkRequest request2 : inventoryOrganization.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToSupplierWorkRequest) {
                        requestIToISP = (InventoryToSupplierWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = requestIToISP.getSender().getUsername();
                        row[1] = requestIToISP.getMedicalDevice().getRfid();
                        row[2] = requestIToISP.getMedicalDevice().getDeviceName();
                        row[3] = requestIToISP.getMedicalDevice().getQuantityNeeded();
                        row[4] = requestIToISP.getStatus();
                        String result = requestIToISP.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToISP;
                        dtm.addRow(row);
                    }
                }
                break;

            case Hospital:
                for (WorkRequest request2 : inventoryOrganization.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof DepartmentToHospInventoryWorkReuest) {
                        requestHTMIToH = (DepartmentToHospInventoryWorkReuest) request2;
                        Object[] row = new Object[7];
                        row[0] = requestHTMIToH.getSender().getUsername();
                        row[1] = requestHTMIToH.getMedicalDevice().getRfid();
                        row[2] = requestHTMIToH.getMedicalDevice().getDeviceName();
                        row[3] = requestHTMIToH.getMedicalDevice().getQuantityNeeded();
                        row[4] = requestHTMIToH.getStatus();
                        String result = requestHTMIToH.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestHTMIToH;
                        dtm.addRow(row);
                    }
                }
                break;
            case Manufacturer:
                for (WorkRequest request2 : inventoryOrganization.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToInventoryWorkRequest) {
                        requestIToI = (InventoryToInventoryWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = requestIToI.getSender().getUsername();
                        row[1] = requestIToI.getMedicalDevice().getRfid();
                        row[2] = requestIToI.getMedicalDevice().getDeviceName();
                        row[3] = requestIToI.getMedicalDevice().getQuantityNeeded();
                        row[4] = requestIToI.getStatus();
                        String result = requestIToI.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToI;
                        dtm.addRow(row);
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblInvTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblManfDeviceCatalog = new javax.swing.JTable();
        lblInvWorkReq = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRequest = new javax.swing.JTable();
        btnRespond = new javax.swing.JButton();
        btnBack2 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Process Request", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        lblInvTitle.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblInvTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInvTitle.setText("Medical Device Catalog");

        tblManfDeviceCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RfId", "Device Code", "Device Name", "Tested", "Quantity Available"
            }
        ));
        jScrollPane1.setViewportView(tblManfDeviceCatalog);

        lblInvWorkReq.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblInvWorkReq.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInvWorkReq.setText("Work Requests");

        tblRequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Device RfID", "Device Name", "Quantity Needed", "Status", "Message", "Result"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblRequest);

        btnRespond.setBackground(new java.awt.Color(51, 122, 183));
        btnRespond.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRespond.setForeground(new java.awt.Color(255, 255, 255));
        btnRespond.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Response_20px.png"))); // NOI18N
        btnRespond.setText("Respond");
        btnRespond.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRespondActionPerformed(evt);
            }
        });

        btnBack2.setBackground(new java.awt.Color(51, 122, 183));
        btnBack2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack2.setForeground(new java.awt.Color(255, 255, 255));
        btnBack2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack2.setText("Back");
        btnBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
                    .addComponent(lblInvTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblInvWorkReq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnBack2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRespond, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInvTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblInvWorkReq)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRespond, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(55, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRespondActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRespondActionPerformed
        // TODO add your handling code here:
        if (tblRequest.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No Requests Found", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else {
            int row = tblRequest.getSelectedRow();

            if (row < 0) {
                JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
                return;
            }

            InventoryToSupplierWorkRequest requestIToI = null;
            DepartmentToHospInventoryWorkReuest requestIToS = null;
            InventoryToInventoryWorkRequest requestMIToPS = null;
            boolean deviceFound = false;
            WorkRequest r1 = (WorkRequest) tblRequest.getValueAt(row, 5);
            switch (enterprise.getEnterpriseType()) {
                case Distributor:
                    if (r1 instanceof InventoryToSupplierWorkRequest) {
                        requestIToI = (InventoryToSupplierWorkRequest) r1;
                        if (requestIToI.getResult() == null) {
                            MedicalDevice d = (MedicalDevice) requestIToI.getMedicalDevice();
                            int sendQty = d.getQuantityNeeded();
                            for (MedicalDevice d2h : inventoryOrganization.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                                if (d2h.getDeviceName().equals(d.getDeviceName())) {
                                    deviceFound = true;
                                    if (sendQty <= d2h.getQuantityAvailable()) {
                                        int beforeSendQty = d2h.getQuantityAvailable();
                                        int afterSendQty = beforeSendQty - sendQty;
                                        d2h.setQuantityAvailable(afterSendQty);
                                        UserAccount ua = requestIToI.getSender();
                                        populateTable();
                                        for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                                            for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                                                if (o.getUserAccountDirectory().getUserAccountList().contains(ua)) {
                                                    if (o instanceof SupplierOrganization) {
                                                        rqtSenderDeviceCatalog = ((SupplierOrganization) o).getMedicalDeviceCatalog();
                                                        enterpriseDeviceCatalog = e.getMedicalDeviceCatalog();
                                                    }
                                                }
                                            }
                                        }
                                        if (rqtSenderDeviceCatalog.getMedicalDeviceList().size() > 0) {
                                            if (!rqtSenderDeviceCatalog.isDeviceCodePresent(d.getDeviceCode())) {
                                                rqtSenderDeviceCatalog.addMedicalDevice(d);
                                            }
                                        } else {
                                            rqtSenderDeviceCatalog.addMedicalDevice(d);
                                        }
                                        for (MedicalDevice md : rqtSenderDeviceCatalog.getMedicalDeviceList()) {
                                            if (md == d) {
                                                int old = md.getQuantityAvailable();
                                                md.setQuantityAvailable(old + sendQty);
                                            }
                                        }
                                        enterpriseDeviceCatalog = rqtSenderDeviceCatalog;
                                        requestIToI.setStatus("Processed");
                                        requestIToI.setMessage("Please check your Inventory. Medical Device shipped from our side");
                                        requestIToI.setResult("Approved");
                                        JOptionPane.showMessageDialog(null, "Medical Device Shipped", "Information", JOptionPane.INFORMATION_MESSAGE);
                                        populateWorkRequestTable();
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Not enough in Inventory", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                }
                            }
                            if (!deviceFound) {
                                JOptionPane.showMessageDialog(null, "Device not in inventory.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else if (requestIToI.getResult().equalsIgnoreCase("Approved")) {
                            JOptionPane.showMessageDialog(null, "Request already approved.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    break;
                case Hospital:
                    if (r1 instanceof DepartmentToHospInventoryWorkReuest) {
                        requestIToS = (DepartmentToHospInventoryWorkReuest) r1;
                        if (requestIToS.getResult() == null) {
                            MedicalDevice d = (MedicalDevice) requestIToS.getMedicalDevice();
                            int sendQty = d.getQuantityNeeded();
                            for (MedicalDevice d2h : inventoryOrganization.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                                if (d2h.getDeviceName().equals(d.getDeviceName())) {
                                    deviceFound = true;
                                    if (sendQty <= d2h.getQuantityAvailable()) {
                                        int beforeSendQty = d2h.getQuantityAvailable();
                                        int afterSendQty = beforeSendQty - sendQty;
                                        d2h.setQuantityAvailable(afterSendQty);
                                        UserAccount ua = requestIToS.getSender();
                                        populateTable();
                                        for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                                            for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                                                if (o.getUserAccountDirectory().getUserAccountList().contains(ua)) {
                                                    if (o instanceof MaintenanceDepartmentOrganization) {
                                                        enterpriseDeviceCatalog = e.getMedicalDeviceCatalog();
                                                        rqtSenderDeviceCatalog = ((MaintenanceDepartmentOrganization) o).getDeviceCatalog();
                                                    }
                                                }
                                            }
                                        }
                                        if (rqtSenderDeviceCatalog.getMedicalDeviceList().size() > 0) {
                                            if (!rqtSenderDeviceCatalog.isDeviceCodePresent(d.getDeviceCode())) {
                                                rqtSenderDeviceCatalog.addMedicalDevice(d);
                                            }
                                        } else {
                                            rqtSenderDeviceCatalog.addMedicalDevice(d);
                                        }
                                        for (MedicalDevice md : rqtSenderDeviceCatalog.getMedicalDeviceList()) {
                                            if (md == d) {
                                                int old = md.getQuantityAvailable();
                                                md.setQuantityAvailable(old + sendQty);
                                            }
                                        }
                                        enterpriseDeviceCatalog = rqtSenderDeviceCatalog;
                                        requestIToS.setStatus("Processed");
                                        requestIToS.setMessage("Please check your Inventory. Medical Device shipped from our side");
                                        requestIToS.setResult("Approved");
                                        JOptionPane.showMessageDialog(null, "Medical Device Shipped", "Information", JOptionPane.INFORMATION_MESSAGE);
                                        populateWorkRequestTable();
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Not enough in Inventory", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                }
                            }
                            if (!deviceFound) {
                                JOptionPane.showMessageDialog(null, "Device not in inventory.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else if (requestIToS.getResult().equalsIgnoreCase("Approved")) {
                            JOptionPane.showMessageDialog(null, "Request already approved.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    break;
                case Manufacturer:
                    if (r1 instanceof InventoryToInventoryWorkRequest) {
                        requestMIToPS = (InventoryToInventoryWorkRequest) r1;
                        if (requestMIToPS.getResult() == null) {
                            MedicalDevice d = (MedicalDevice) requestMIToPS.getMedicalDevice();
                            int sendQty = d.getQuantityNeeded();
                            for (MedicalDevice d2h : enterprise.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                                if (d2h.getDeviceName().equals(d.getDeviceName())) {
                                    deviceFound = true;
                                    if (d2h.isTestedOk()) {
                                        if (sendQty <= d2h.getQuantityAvailable()) {
                                            int beforeSendQty = d2h.getQuantityAvailable();
                                            int afterSendQty = beforeSendQty - sendQty;
                                            d2h.setQuantityAvailable(afterSendQty);
                                            UserAccount ua = requestMIToPS.getSender();
                                            populateTable();
                                            for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                                                for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                                                    if (o.getUserAccountDirectory().getUserAccountList().contains(ua)) {
                                                        if (o instanceof InventoryOrganization) {
                                                            rqtSenderDeviceCatalog = ((InventoryOrganization) o).getMedicalDeviceCatalog();
                                                        }
                                                    }
                                                }
                                            }
                                            if (rqtSenderDeviceCatalog.getMedicalDeviceList().size() > 0) {
                                                if (!rqtSenderDeviceCatalog.isDeviceCodePresent(d.getDeviceCode())) {
                                                    rqtSenderDeviceCatalog.addMedicalDevice(d);
                                                }
                                            } else {
                                                rqtSenderDeviceCatalog.addMedicalDevice(d);
                                            }
                                            for (MedicalDevice md : rqtSenderDeviceCatalog.getMedicalDeviceList()) {
                                                if (md == d) {
                                                    int old = md.getQuantityAvailable();
                                                    md.setQuantityAvailable(old + sendQty);
                                                }
                                            }
                                            requestMIToPS.setStatus("Processed");
                                            requestMIToPS.setMessage("Please check your Inventory. Medical Device shipped from our side");
                                            requestMIToPS.setResult("Approved");
                                            JOptionPane.showMessageDialog(null, "Medical Device Shipped", "Information", JOptionPane.INFORMATION_MESSAGE);
                                            populateWorkRequestTable();
                                        } else {
                                            JOptionPane.showMessageDialog(null, "Not enough in Inventory", "Error", JOptionPane.ERROR_MESSAGE);
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Device is under testing. Request cannot be processed.", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                }
                            }
                            if (!deviceFound) {
                                JOptionPane.showMessageDialog(null, "Device not in inventory.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else if (requestMIToPS.getResult().equalsIgnoreCase("Approved")) {
                            JOptionPane.showMessageDialog(null, "Request already approved.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }//GEN-LAST:event_btnRespondActionPerformed

    private void btnBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack2ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack2;
    private javax.swing.JButton btnRespond;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblInvTitle;
    private javax.swing.JLabel lblInvWorkReq;
    private javax.swing.JTable tblManfDeviceCatalog;
    private javax.swing.JTable tblRequest;
    // End of variables declaration//GEN-END:variables
}
