/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Inventory;

import Business.Enterprise.Enterprise;
import Business.MedicalDevice.MedicalDevice;
import Business.MedicalDevice.MedicalDeviceCatalog;
import Business.Network.State;
import Business.Organization.DeviceProductionOrganization;
import Business.Organization.InventoryOrganization;
import Business.Organization.Organization;
import Business.Organization.SupplierOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.InventoryToInventoryWorkRequest;
import Business.WorkQueue.InventoryToSupplierWorkRequest;
import Business.WorkQueue.MnfInvToDeviceProdWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Harsh
 */
public class ManageWorkRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageWorkRequestJPanel
     */
    JPanel userProcessContainer;
    InventoryOrganization organization;
    UserAccount userAccount;
    Enterprise enterprise;
    State state;
    private MedicalDeviceCatalog rqtSenderDeviceCatalog;

    public ManageWorkRequestJPanel(JPanel userProcessContainer, UserAccount account, InventoryOrganization organization, State state, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.organization = organization;
        this.enterprise = enterprise;
        this.state = state;
        lblInvTitle.setText(enterprise.getEnterpriseType() + " Inventory Organization Medical Device Catalog");
        lblInvWorkReq.setText(enterprise.getEnterpriseType() + " Inventory Organization Work Requests");
        populateTable();
        populateWorkRequestTable();
    }

    public void populateTable() {
        DefaultTableModel dtm = (DefaultTableModel) tblManfDeviceCatalog.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblManfDeviceCatalog.setRowSorter(sorter);
        dtm.setRowCount(0);
        if (!enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Manufacturer)) {
            for (MedicalDevice md : organization.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                Object row[] = new Object[5];
                row[0] = md.getRfid();
                row[1] = md;
                row[2] = md.getDeviceName();
                row[3] = md.isTestedOk();
                row[4] = md.getQuantityAvailable();
                dtm.addRow(row);
            }
        } else {
            for (MedicalDevice md : enterprise.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                Object row[] = new Object[5];
                row[0] = md.getRfid();
                row[1] = md;
                row[2] = md.getDeviceName();
                row[3] = md.isTestedOk();
                row[4] = md.getQuantityAvailable();
                dtm.addRow(row);
            }
        }
    }

    public void populateWorkRequestTable() {

        DefaultTableModel dtm = (DefaultTableModel) tblRequest.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblRequest.setRowSorter(sorter);

        InventoryToInventoryWorkRequest requestIToI = null;
        InventoryToSupplierWorkRequest requestIToS = null;
        MnfInvToDeviceProdWorkRequest requestMIToPS = null;
        dtm.setRowCount(0);
        switch (enterprise.getEnterpriseType()) {
            case Distributor:
                for (WorkRequest request2 : userAccount.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToInventoryWorkRequest) {
                        requestIToI = (InventoryToInventoryWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = userAccount;
                        row[1] = requestIToI.getMedicalDevice().getRfid();
                        row[2] = requestIToI.getMedicalDevice().getDeviceName();
                        row[3] = requestIToI.getDeviceQtyNeeded();
                        row[4] = requestIToI.getStatus();
                        String result = requestIToI.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToI;
                        dtm.addRow(row);
                    }
                }
                break;

            case Hospital:
                for (WorkRequest request2 : userAccount.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToSupplierWorkRequest) {
                        requestIToS = (InventoryToSupplierWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = userAccount;
                        row[1] = requestIToS.getMedicalDevice().getRfid();
                        row[2] = requestIToS.getMedicalDevice().getDeviceName();
                        row[3] = requestIToS.getDeviceQtyNeeded();
                        row[4] = requestIToS.getStatus();
                        String result = requestIToS.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToS;
                        dtm.addRow(row);
                    }
                }
                break;
            case Manufacturer:
                for (WorkRequest request2 : userAccount.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof MnfInvToDeviceProdWorkRequest) {
                        requestMIToPS = (MnfInvToDeviceProdWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = userAccount;
                        row[1] = requestMIToPS.getMedicalDevice().getRfid();
                        row[2] = requestMIToPS.getMedicalDevice().getDeviceName();
                        row[3] = requestMIToPS.getMedicalDevice().getQuantityNeeded();
                        row[4] = requestMIToPS.getStatus();
                        String result = requestMIToPS.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestMIToPS;
                        dtm.addRow(row);
                    } 
                }
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblInvTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblManfDeviceCatalog = new javax.swing.JTable();
        requestTestJButton = new javax.swing.JButton();
        lblInvWorkReq = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRequest = new javax.swing.JTable();
        btnBack2 = new javax.swing.JButton();
        quantitySpinner = new javax.swing.JSpinner();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Manage Work Request", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        lblInvTitle.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblInvTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInvTitle.setText("Medical Device Catalog");

        tblManfDeviceCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RfId", "Device Code", "Device Name", "Tested", "Quantity Available"
            }
        ));
        jScrollPane1.setViewportView(tblManfDeviceCatalog);

        requestTestJButton.setBackground(new java.awt.Color(51, 122, 183));
        requestTestJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        requestTestJButton.setForeground(new java.awt.Color(255, 255, 255));
        requestTestJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Validation_20px.png"))); // NOI18N
        requestTestJButton.setText("Request Device");
        requestTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTestJButtonActionPerformed(evt);
            }
        });

        lblInvWorkReq.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblInvWorkReq.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInvWorkReq.setText("Work Requests");

        tblRequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Device RfID", "Device Name", "Quantity Needed", "Status", "Message", "Result"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblRequest);

        btnBack2.setBackground(new java.awt.Color(51, 122, 183));
        btnBack2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack2.setForeground(new java.awt.Color(255, 255, 255));
        btnBack2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack2.setText("Back");
        btnBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack2ActionPerformed(evt);
            }
        });

        quantitySpinner.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(235, 414, Short.MAX_VALUE)
                        .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(requestTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2)
                    .addComponent(lblInvWorkReq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(lblInvTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnBack2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblInvTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblInvWorkReq)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void requestTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTestJButtonActionPerformed

        if (tblManfDeviceCatalog.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No device available in the inventory. Request from manufacturer list.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        int selectedRow = tblManfDeviceCatalog.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        int fetchedQty = Integer.parseInt(String.valueOf(quantitySpinner.getValue()));
        if (fetchedQty == 0) {
            JOptionPane.showMessageDialog(this, "Request atleast 1 unit", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (fetchedQty > 5) {
            JOptionPane.showMessageDialog(this, "Cannot request more then 5 device at once.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        MedicalDevice selectedMedicalDevice = (MedicalDevice) tblManfDeviceCatalog.getValueAt(selectedRow, 1);
        selectedMedicalDevice.setQuantityNeeded(fetchedQty);
        InventoryToInventoryWorkRequest requestIToI = null;
        InventoryToSupplierWorkRequest requestIToS = null;
        MnfInvToDeviceProdWorkRequest requestMIToPS = null;
        Organization org = null;
        switch (enterprise.getEnterpriseType()) {
            case Distributor:
                requestIToI = new InventoryToInventoryWorkRequest();
                requestIToI.setSender(userAccount);
                requestIToI.setStatus("Request Generated");
                requestIToI.setMessage("Medical Device needed.");
                requestIToI.setMedicalDevice(selectedMedicalDevice);
                requestIToI.setDeviceQtyNeeded(fetchedQty);
                for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                    if (e.getEnterpriseType().equals(Enterprise.EnterpriseType.Manufacturer)) {
                        for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                            if (o instanceof InventoryOrganization) {
                                org = o;
                                break;
                            }
                        }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(requestIToI);
                    userAccount.getWorkQueue().getWorkRequestList().add(requestIToI);
                } else {
                    JOptionPane.showMessageDialog(this, "No manufacturer inventory created.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                break;
            case Hospital:
                requestIToS = new InventoryToSupplierWorkRequest();
                requestIToS.setSender(userAccount);
                requestIToS.setStatus("Request Generated");
                requestIToS.setMessage("Medical Device needed.");
                requestIToS.setMedicalDevice(selectedMedicalDevice);
                requestIToS.setDeviceQtyNeeded(fetchedQty);
                for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                    if (e.getEnterpriseType().equals(Enterprise.EnterpriseType.Distributor)) {
                        for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                            if (o instanceof SupplierOrganization) {
                                org = o;
                                break;
                            }
                        }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(requestIToS);
                    userAccount.getWorkQueue().getWorkRequestList().add(requestIToS);
                } else {
                    JOptionPane.showMessageDialog(this, "No supplier is created.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                break;
            case Manufacturer:
                requestMIToPS = new MnfInvToDeviceProdWorkRequest();
                requestMIToPS.setSender(userAccount);
                requestMIToPS.setStatus("Request Generated");
                requestMIToPS.setMessage("Medical Device needed.");
                requestMIToPS.setMedicalDevice(selectedMedicalDevice);
                for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof DeviceProductionOrganization) {
                            org = o;
                            break;
                        }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(requestMIToPS);
                    userAccount.getWorkQueue().getWorkRequestList().add(requestMIToPS);
                } else {
                    JOptionPane.showMessageDialog(this, "No production area is created.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                break;
            default:
                break;
        }
        if (org != null) {
            JOptionPane.showMessageDialog(this, "Request sent successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        populateWorkRequestTable();
    }//GEN-LAST:event_requestTestJButtonActionPerformed

    private void btnBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack2ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblInvTitle;
    private javax.swing.JLabel lblInvWorkReq;
    private javax.swing.JSpinner quantitySpinner;
    private javax.swing.JButton requestTestJButton;
    private javax.swing.JTable tblManfDeviceCatalog;
    private javax.swing.JTable tblRequest;
    // End of variables declaration//GEN-END:variables
}
