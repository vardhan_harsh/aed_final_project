/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Inventory;

import Business.Enterprise.Enterprise;
import Business.MedicalDevice.MedicalDevice;
import Business.Network.State;
import Business.Organization.InventoryOrganization;
import Business.Organization.Organization;
import Business.Organization.SupplierOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.InventoryToInventoryWorkRequest;
import Business.WorkQueue.InventoryToSupplierWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Harsh
 */
public class RequestDeviceFromManufactureListJPanel extends javax.swing.JPanel {

    /**
     * Creates new form RequestDeviceFromManufactureListJPanel
     */
    JPanel userProcessContainer;
    InventoryOrganization organization;
    UserAccount account;
    Enterprise enterprise;
    State state;

    public RequestDeviceFromManufactureListJPanel(JPanel userProcessContainer, UserAccount account, InventoryOrganization organization, Enterprise enterprise, State state) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.organization = organization;
        this.enterprise = enterprise;
        this.state = state;
        cbDeviceName.removeAllItems();
        popuplateDeviceName();
        populateWorkRequestTable();
    }

    private void popuplateDeviceName() {
        cbDeviceName.removeAllItems();
        cbDeviceName.addItem("--Select--");
        for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
            if (e.getEnterpriseType().equals(Enterprise.EnterpriseType.Manufacturer)) {
                for (MedicalDevice md : e.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                    boolean isPresent = false;
                    for (MedicalDevice m : organization.getMedicalDeviceCatalog().getMedicalDeviceList()) {
                        if (m.getDeviceCode().equalsIgnoreCase(md.getDeviceCode())) {
                            isPresent = true;
                        }
                    }
                    if (!isPresent) {
                        cbDeviceName.addItem(md);
                    }
                }
            }
        }
    }

    public void populateWorkRequestTable() {

        DefaultTableModel dtm = (DefaultTableModel) tblRequest.getModel();
        //Sort the Table according to the column name seleced.
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(dtm);
        tblRequest.setRowSorter(sorter);

        InventoryToInventoryWorkRequest requestIToI = null;
        InventoryToSupplierWorkRequest requestIToS = null;
        dtm.setRowCount(0);
        switch (enterprise.getEnterpriseType()) {
            case Distributor:
                for (WorkRequest request2 : account.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToInventoryWorkRequest) {
                        requestIToI = (InventoryToInventoryWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = account;
                        row[1] = requestIToI.getMedicalDevice().getRfid();
                        row[2] = requestIToI.getMedicalDevice().getDeviceName();
                        row[3] = requestIToI.getDeviceQtyNeeded();
                        row[4] = requestIToI.getStatus();
                        String result = requestIToI.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToI;
                        dtm.addRow(row);
                    }
                }
                break;

            case Hospital:
                for (WorkRequest request2 : account.getWorkQueue().getWorkRequestList()) {
                    if (request2 instanceof InventoryToSupplierWorkRequest) {
                        requestIToS = (InventoryToSupplierWorkRequest) request2;
                        Object[] row = new Object[7];
                        row[0] = account;
                        row[1] = requestIToS.getMedicalDevice().getRfid();
                        row[2] = requestIToS.getMedicalDevice().getDeviceName();
                        row[3] = requestIToS.getDeviceQtyNeeded();
                        row[4] = requestIToS.getStatus();
                        String result = requestIToS.getResult();
                        row[6] = result == null ? "Waiting" : result;
                        row[5] = requestIToS;
                        dtm.addRow(row);
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDevice = new javax.swing.JLabel();
        cbDeviceName = new javax.swing.JComboBox();
        jsQuantity = new javax.swing.JSpinner();
        requestTestJButton = new javax.swing.JButton();
        btnBack7 = new javax.swing.JButton();
        lblDevice1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRequest = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Request Medical Device", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        lblDevice.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDevice.setText("Medical Device");

        cbDeviceName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        requestTestJButton.setBackground(new java.awt.Color(51, 122, 183));
        requestTestJButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        requestTestJButton.setForeground(new java.awt.Color(255, 255, 255));
        requestTestJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Request_Service_20px.png"))); // NOI18N
        requestTestJButton.setText("Request Device ");
        requestTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTestJButtonActionPerformed(evt);
            }
        });

        btnBack7.setBackground(new java.awt.Color(51, 122, 183));
        btnBack7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack7.setForeground(new java.awt.Color(255, 255, 255));
        btnBack7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack7.setText("Back");
        btnBack7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack7ActionPerformed(evt);
            }
        });

        lblDevice1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDevice1.setText("Quantity");

        tblRequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sender", "Device RfID", "Device Name", "Quantity Needed", "Status", "Message", "Result"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblRequest);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblDevice1)
                                    .addComponent(lblDevice)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack7)
                                .addGap(0, 247, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbDeviceName, 0, 200, Short.MAX_VALUE)
                            .addComponent(requestTestJButton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(jsQuantity, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
                        .addGap(336, 336, 336))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDevice1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jsQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestTestJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(135, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void requestTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTestJButtonActionPerformed
        MedicalDevice device = null;
        if (cbDeviceName.getSelectedIndex() > 0) {
            device = (MedicalDevice) cbDeviceName.getSelectedItem();
        } else {
            JOptionPane.showMessageDialog(null, "Please select a device from device list", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        int fetchedQty = Integer.parseInt(String.valueOf(jsQuantity.getValue()));
        if (fetchedQty == 0) {
            JOptionPane.showMessageDialog(this, "Request atleast 1 unit", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (fetchedQty > 5) {
            JOptionPane.showMessageDialog(this, "Cannot request more then 5 device at once.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        device.setQuantityNeeded(fetchedQty);
        InventoryToInventoryWorkRequest requestIToI = null;
        InventoryToSupplierWorkRequest requestIToS = null;
        Organization org = null;
        switch (enterprise.getEnterpriseType()) {
            case Distributor:
                requestIToI = new InventoryToInventoryWorkRequest();
                MedicalDevice mdDist = new MedicalDevice(true);
                mapDevice(device, mdDist);
                mdDist.setQuantityNeeded(Integer.parseInt(String.valueOf(jsQuantity.getValue())));
                requestIToI.setMedicalDevice(mdDist);
                requestIToI.setDeviceQtyNeeded(fetchedQty);
                requestIToI.setSender(account);
                requestIToI.setStatus("Request Generated");
                requestIToI.setMessage("Medical Device needed.");
                for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                    if (e.getEnterpriseType().equals(Enterprise.EnterpriseType.Manufacturer)) {
                        for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                            if (o instanceof InventoryOrganization) {
                                org = o;
                                break;
                            }
                        }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(requestIToI);
                    account.getWorkQueue().getWorkRequestList().add(requestIToI);
                } else {
                    JOptionPane.showMessageDialog(this, "No manufacturer inventory created.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                break;
            case Hospital:
                requestIToS = new InventoryToSupplierWorkRequest();
                MedicalDevice mdHosp = new MedicalDevice(true);
                mapDevice(device, mdHosp);
                mdHosp.setQuantityNeeded(Integer.parseInt(String.valueOf(jsQuantity.getValue())));
                requestIToS.setMedicalDevice(mdHosp);
                requestIToS.setDeviceQtyNeeded(fetchedQty);
                requestIToS.setSender(account);
                requestIToS.setStatus("Request Generated");
                requestIToS.setMessage("Medical Device needed.");
                for (Enterprise e : state.getEnterpriseDirectory().getEnterpriseList()) {
                    if (e.getEnterpriseType().equals(Enterprise.EnterpriseType.Distributor)) {
                        for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                            if (o instanceof SupplierOrganization) {
                                org = o;
                                break;
                            }
                        }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(requestIToS);
                    account.getWorkQueue().getWorkRequestList().add(requestIToS);
                } else {
                    JOptionPane.showMessageDialog(this, "No supplier is created.", "Warning", JOptionPane.WARNING_MESSAGE);
                }
                break;
            default:
                break;
        }
        if (org != null) {
            JOptionPane.showMessageDialog(null, "Request message sent");
        }
        populateWorkRequestTable();
    }//GEN-LAST:event_requestTestJButtonActionPerformed

    private void btnBack7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack7ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack7ActionPerformed

    public void mapDevice(MedicalDevice device, MedicalDevice md) {
        md.setRfid(device.getRfid());
        md.setDeviceCode(device.getDeviceCode());
        md.setDeviceName(device.getDeviceName());
        md.setPrice(device.getPrice());
        md.setWarrantyDate(device.getWarrantyDate());
        md.setManufacturingDate(device.getManufacturingDate());
        md.setQuantityAvailable(0);
        md.setTestedOk(device.isTestedOk());
        md.setPerformanceAttribute(device.getPerformanceAttribute());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack7;
    private javax.swing.JComboBox cbDeviceName;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jsQuantity;
    private javax.swing.JLabel lblDevice;
    private javax.swing.JLabel lblDevice1;
    private javax.swing.JButton requestTestJButton;
    private javax.swing.JTable tblRequest;
    // End of variables declaration//GEN-END:variables
}
