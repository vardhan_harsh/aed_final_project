/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HTM;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.MedicalDevice.Encounter;
import Business.MedicalDevice.MedicalDevice;
import Business.MedicalDevice.PerformanceAttribute;
import Business.Network.State;
import Business.Utilities.MedicalDeviceTestData;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

/**
 *
 * @author Harsh
 */
public class MedicalDeviceLiveDataJPanel extends javax.swing.JPanel {

    /**
     * Creates new form MedicalDeviceLiveDataJPanel
     */
    JPanel userProcessContainer;
    Enterprise enterprise;
    State state;
    MedicalDevice md;
    MedicalDeviceTestData testDate;
    Timer timer1;
    EcoSystem system;

    public MedicalDeviceLiveDataJPanel(JPanel userProcessContainer, Enterprise enterprise, State state, MedicalDevice md, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.state = state;
        this.system = system;
        this.md = md;
        testDate = new MedicalDeviceTestData(md.getPerformanceAttribute().getDeviceRegulatoryClass(), system.getCountryName(state));
        jTabbedPane1.setIconAt(0, new ImageIcon(getClass().getResource("/Images/icons8_Information_20px.png")));
        jTabbedPane1.setIconAt(1, new ImageIcon(getClass().getResource("/Images/icons8_Order_History_20px.png")));
        jTabbedPane1.setIconAt(2, new ImageIcon(getClass().getResource("/Images/icons8_Maintenance_20px_1.png")));
        jTabbedPane1.setIconAt(3, new ImageIcon(getClass().getResource("/Images/icons8_Temperature_20px.png")));
        jTabbedPane1.setIconAt(4, new ImageIcon(getClass().getResource("/Images/icons8_Frequency_20px.png")));
        jTabbedPane1.setIconAt(5, new ImageIcon(getClass().getResource("/Images/icons8_Audio_Wave_20px.png")));
        jTabbedPane1.setIconAt(6, new ImageIcon(getClass().getResource("/Images/icons8_Weight_20px_1.png")));
        jTabbedPane1.setIconAt(7, new ImageIcon(getClass().getResource("/Images/icons8_Health_Book_20px_2.png")));
        jTabbedPane1.setIconAt(8, new ImageIcon(getClass().getResource("/Images/icons8_System_Report_20px.png")));
        jTabbedPane1.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent ce) {
                if (jTabbedPane1.getSelectedComponent().equals(DeviceHistory)) {
                    deviceHistory();
                } else if (jTabbedPane1.getSelectedComponent().equals(DeviceEncounter)) {
                    deviceEncounter();
                } else if (jTabbedPane1.getSelectedComponent().equals(Temp)) {
                    createTemperatureChart(panelTemp, false);
                } else if (jTabbedPane1.getSelectedComponent().equals(Frq)) {
                    createFrequencyChart(panelFrq, false);
                } else if (jTabbedPane1.getSelectedComponent().equals(PSD)) {
                    createPSDChart(panelPSD, false);
                } else if (jTabbedPane1.getSelectedComponent().equals(Load)) {
                    createLoadChart(panelLoad, false);
                } else if (jTabbedPane1.getSelectedComponent().equals(Health)) {
                    createHealthChart();
                } else if (jTabbedPane1.getSelectedComponent().equals(Report)) {
                    createTemperatureChart(pnlTemp, true);
                    createFrequencyChart(pnlFrq, true);
                    createPSDChart(pnlPSD, true);
                    createLoadChart(pnlLoad, true);
                }
            }
        });
        populateMedicalDeviceAttributes();
    }

    private void populateMedicalDeviceAttributes() {
        txtDeviceCode.setText(md.getDeviceCode());
        cbDeviceName.setSelectedItem(md.getDeviceName());
        txtPrice.setText(String.valueOf(md.getPrice()));
        jDthMnfDate.setDate(md.getManufacturingDate());
        jDtExpireDate.setDate(md.getWarrantyDate());
        txtQuantity.setValue(md.getQuantityAvailable());
        txtTemp.setText(String.valueOf(md.getPerformanceAttribute().getTemperature()));
        txtFrq.setText(String.valueOf(md.getPerformanceAttribute().getFrequency()));
        txtPSD.setText(String.valueOf(md.getPerformanceAttribute().getPowerSpectralDensity()));
        txtLoad.setText(String.valueOf(md.getPerformanceAttribute().getLoad()));
        txtChargeTime.setText(String.valueOf(md.getPerformanceAttribute().getChargeTime()));
        txtDRC.setText(String.valueOf(md.getPerformanceAttribute().getDeviceRegulatoryClass()));
    }

    private void deviceEncounter() {
        int rowCount = tbEncounter.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tbEncounter.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        for (Encounter ec : md.getEncounterHistory().getEncounterList()) {
            Object row[] = new Object[3];
            row[0] = ec.isFailure();
            row[1] = ec.isMaintenance();
            row[2] = ec.getEncounterTimestamp();
            model.addRow(row);
        }
    }

    private void deviceHistory() {
        int rowCount = tblPerformanceHistory.getRowCount();

        DefaultTableModel model = (DefaultTableModel) tblPerformanceHistory.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (PerformanceAttribute pa : md.getPerformanceHistory().getPerformanceHistoryAttributeList()) {
            Object row[] = new Object[6];
            row[0] = pa.getTemperature();
            row[1] = pa.getFrequency();
            row[2] = pa.getPowerSpectralDensity();
            row[3] = pa.getLoad();
            row[4] = pa.getHealth();
            row[5] = pa.getSensorTimestamp();
            model.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelDGI = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDeviceCode = new javax.swing.JTextField();
        cbDeviceName = new javax.swing.JComboBox<>();
        txtPrice = new javax.swing.JTextField();
        jDthMnfDate = new com.toedter.calendar.JDateChooser();
        jDtExpireDate = new com.toedter.calendar.JDateChooser();
        txtQuantity = new javax.swing.JSpinner();
        jPanel8 = new javax.swing.JPanel();
        txtChargeTime = new javax.swing.JTextField();
        txtLoad = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtTemp = new javax.swing.JTextField();
        txtFrq = new javax.swing.JTextField();
        txtPSD = new javax.swing.JTextField();
        txtDRC = new javax.swing.JTextField();
        btnBack8 = new javax.swing.JButton();
        DeviceHistory = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPerformanceHistory = new javax.swing.JTable();
        btnShowPerformanceChart = new javax.swing.JButton();
        btnBack7 = new javax.swing.JButton();
        DeviceEncounter = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbEncounter = new javax.swing.JTable();
        btnShowEncounterChart = new javax.swing.JButton();
        btnBack6 = new javax.swing.JButton();
        Temp = new javax.swing.JPanel();
        panelTemp = new javax.swing.JPanel();
        btnRefreshTemp = new javax.swing.JButton();
        btnBack5 = new javax.swing.JButton();
        Frq = new javax.swing.JPanel();
        panelFrq = new javax.swing.JPanel();
        btnRefreshFrq = new javax.swing.JButton();
        btnBack4 = new javax.swing.JButton();
        PSD = new javax.swing.JPanel();
        panelPSD = new javax.swing.JPanel();
        btnRefreshPSD = new javax.swing.JButton();
        btnBack3 = new javax.swing.JButton();
        Load = new javax.swing.JPanel();
        panelLoad = new javax.swing.JPanel();
        btnRefreshLoad = new javax.swing.JButton();
        btnBack2 = new javax.swing.JButton();
        Health = new javax.swing.JPanel();
        panelHealth = new javax.swing.JPanel();
        btnRefreshHealth = new javax.swing.JButton();
        btnBack1 = new javax.swing.JButton();
        Report = new javax.swing.JPanel();
        pnlTemp = new javax.swing.JPanel();
        pnlFrq = new javax.swing.JPanel();
        pnlPSD = new javax.swing.JPanel();
        pnlLoad = new javax.swing.JPanel();
        btnLive = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device Live Data", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jTabbedPane1.setBackground(new java.awt.Color(51, 122, 183));
        jTabbedPane1.setForeground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTabbedPane1.setOpaque(true);

        panelDGI.setBackground(new java.awt.Color(255, 255, 255));
        panelDGI.setEnabled(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device General Attributes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Device Code");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Device Name");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Price");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setText("Manufacturing Date");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Warranty Exprire Date");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Quantity");

        txtDeviceCode.setEnabled(false);

        cbDeviceName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbDeviceName.setEnabled(false);

        txtPrice.setEnabled(false);

        jDthMnfDate.setBackground(new java.awt.Color(255, 255, 255));
        jDthMnfDate.setEnabled(false);

        jDtExpireDate.setBackground(new java.awt.Color(255, 255, 255));
        jDtExpireDate.setEnabled(false);

        txtQuantity.setModel(new javax.swing.SpinnerNumberModel());
        txtQuantity.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDeviceCode, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jDthMnfDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDtExpireDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDthMnfDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDtExpireDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDeviceCode, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device General Attributes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txtChargeTime.setEnabled(false);

        txtLoad.setEnabled(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Temprature");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Frequency");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Power Spectral Density");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Device Regulatory Class");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Charge Time");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Load");

        txtTemp.setEnabled(false);

        txtFrq.setEnabled(false);

        txtPSD.setEnabled(false);

        txtDRC.setEnabled(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTemp)
                    .addComponent(txtFrq)
                    .addComponent(txtPSD, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(txtLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(txtChargeTime, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(txtDRC, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtChargeTime, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDRC, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTemp, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPSD, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtFrq, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnBack8.setBackground(new java.awt.Color(51, 122, 183));
        btnBack8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack8.setForeground(new java.awt.Color(255, 255, 255));
        btnBack8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack8.setText("Back");
        btnBack8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDGILayout = new javax.swing.GroupLayout(panelDGI);
        panelDGI.setLayout(panelDGILayout);
        panelDGILayout.setHorizontalGroup(
            panelDGILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDGILayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDGILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack8))
                .addContainerGap(46, Short.MAX_VALUE))
            .addGroup(panelDGILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelDGILayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(49, Short.MAX_VALUE)))
        );
        panelDGILayout.setVerticalGroup(
            panelDGILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelDGILayout.createSequentialGroup()
                .addContainerGap(234, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(btnBack8, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(panelDGILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelDGILayout.createSequentialGroup()
                    .addGap(36, 36, 36)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(298, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("General Information  ", panelDGI);

        DeviceHistory.setBackground(new java.awt.Color(255, 255, 255));

        tblPerformanceHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Temprature", "Frequency", "Power Density", "Load", "Health", "Date"
            }
        ));
        jScrollPane1.setViewportView(tblPerformanceHistory);

        btnShowPerformanceChart.setBackground(new java.awt.Color(51, 122, 183));
        btnShowPerformanceChart.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnShowPerformanceChart.setForeground(new java.awt.Color(255, 255, 255));
        btnShowPerformanceChart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Combo_Chart_20px.png"))); // NOI18N
        btnShowPerformanceChart.setText("Show Chart");
        btnShowPerformanceChart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowPerformanceChartActionPerformed(evt);
            }
        });

        btnBack7.setBackground(new java.awt.Color(51, 122, 183));
        btnBack7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack7.setForeground(new java.awt.Color(255, 255, 255));
        btnBack7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack7.setText("Back");
        btnBack7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DeviceHistoryLayout = new javax.swing.GroupLayout(DeviceHistory);
        DeviceHistory.setLayout(DeviceHistoryLayout);
        DeviceHistoryLayout.setHorizontalGroup(
            DeviceHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeviceHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DeviceHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DeviceHistoryLayout.createSequentialGroup()
                        .addComponent(btnBack7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnShowPerformanceChart, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        DeviceHistoryLayout.setVerticalGroup(
            DeviceHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeviceHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(DeviceHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnShowPerformanceChart, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Performance History  ", DeviceHistory);

        DeviceEncounter.setBackground(new java.awt.Color(255, 255, 255));

        tbEncounter.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Failure", "Maintenance", "Timestamp"
            }
        ));
        jScrollPane2.setViewportView(tbEncounter);

        btnShowEncounterChart.setBackground(new java.awt.Color(51, 122, 183));
        btnShowEncounterChart.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnShowEncounterChart.setForeground(new java.awt.Color(255, 255, 255));
        btnShowEncounterChart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Combo_Chart_20px.png"))); // NOI18N
        btnShowEncounterChart.setText("Show Chart");
        btnShowEncounterChart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowEncounterChartActionPerformed(evt);
            }
        });

        btnBack6.setBackground(new java.awt.Color(51, 122, 183));
        btnBack6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack6.setForeground(new java.awt.Color(255, 255, 255));
        btnBack6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack6.setText("Back");
        btnBack6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DeviceEncounterLayout = new javax.swing.GroupLayout(DeviceEncounter);
        DeviceEncounter.setLayout(DeviceEncounterLayout);
        DeviceEncounterLayout.setHorizontalGroup(
            DeviceEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeviceEncounterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DeviceEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DeviceEncounterLayout.createSequentialGroup()
                        .addComponent(btnBack6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnShowEncounterChart, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        DeviceEncounterLayout.setVerticalGroup(
            DeviceEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeviceEncounterLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(DeviceEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnShowEncounterChart, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Device Encounters  ", DeviceEncounter);

        Temp.setBackground(new java.awt.Color(255, 255, 255));

        panelTemp.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Temperature Graph", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout panelTempLayout = new javax.swing.GroupLayout(panelTemp);
        panelTemp.setLayout(panelTempLayout);
        panelTempLayout.setHorizontalGroup(
            panelTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelTempLayout.setVerticalGroup(
            panelTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        btnRefreshTemp.setBackground(new java.awt.Color(51, 122, 183));
        btnRefreshTemp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefreshTemp.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshTemp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        btnRefreshTemp.setText("Refresh");
        btnRefreshTemp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshTempActionPerformed(evt);
            }
        });

        btnBack5.setBackground(new java.awt.Color(51, 122, 183));
        btnBack5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack5.setForeground(new java.awt.Color(255, 255, 255));
        btnBack5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack5.setText("Back");
        btnBack5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout TempLayout = new javax.swing.GroupLayout(Temp);
        Temp.setLayout(TempLayout);
        TempLayout.setHorizontalGroup(
            TempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TempLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TempLayout.createSequentialGroup()
                        .addComponent(btnBack5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 391, Short.MAX_VALUE)
                        .addComponent(btnRefreshTemp, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        TempLayout.setVerticalGroup(
            TempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TempLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefreshTemp, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack5, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Temperature  ", Temp);

        Frq.setBackground(new java.awt.Color(255, 255, 255));

        panelFrq.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Frequency Graph", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout panelFrqLayout = new javax.swing.GroupLayout(panelFrq);
        panelFrq.setLayout(panelFrqLayout);
        panelFrqLayout.setHorizontalGroup(
            panelFrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 664, Short.MAX_VALUE)
        );
        panelFrqLayout.setVerticalGroup(
            panelFrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        btnRefreshFrq.setBackground(new java.awt.Color(51, 122, 183));
        btnRefreshFrq.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefreshFrq.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshFrq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        btnRefreshFrq.setText("Refresh");
        btnRefreshFrq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshFrqActionPerformed(evt);
            }
        });

        btnBack4.setBackground(new java.awt.Color(51, 122, 183));
        btnBack4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack4.setForeground(new java.awt.Color(255, 255, 255));
        btnBack4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack4.setText("Back");
        btnBack4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FrqLayout = new javax.swing.GroupLayout(Frq);
        Frq.setLayout(FrqLayout);
        FrqLayout.setHorizontalGroup(
            FrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrqLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelFrq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FrqLayout.createSequentialGroup()
                        .addComponent(btnBack4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefreshFrq, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        FrqLayout.setVerticalGroup(
            FrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrqLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelFrq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefreshFrq, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Frequency  ", Frq);

        PSD.setBackground(new java.awt.Color(255, 255, 255));

        panelPSD.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Power Spectral Density Graph", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout panelPSDLayout = new javax.swing.GroupLayout(panelPSD);
        panelPSD.setLayout(panelPSDLayout);
        panelPSDLayout.setHorizontalGroup(
            panelPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 664, Short.MAX_VALUE)
        );
        panelPSDLayout.setVerticalGroup(
            panelPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        btnRefreshPSD.setBackground(new java.awt.Color(51, 122, 183));
        btnRefreshPSD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefreshPSD.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshPSD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        btnRefreshPSD.setText("Refresh");
        btnRefreshPSD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshPSDActionPerformed(evt);
            }
        });

        btnBack3.setBackground(new java.awt.Color(51, 122, 183));
        btnBack3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack3.setForeground(new java.awt.Color(255, 255, 255));
        btnBack3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack3.setText("Back");
        btnBack3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PSDLayout = new javax.swing.GroupLayout(PSD);
        PSD.setLayout(PSDLayout);
        PSDLayout.setHorizontalGroup(
            PSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PSDLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelPSD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PSDLayout.createSequentialGroup()
                        .addComponent(btnBack3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefreshPSD, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        PSDLayout.setVerticalGroup(
            PSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PSDLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPSD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefreshPSD, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Power  ", PSD);

        Load.setBackground(new java.awt.Color(255, 255, 255));

        panelLoad.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Load Graph", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout panelLoadLayout = new javax.swing.GroupLayout(panelLoad);
        panelLoad.setLayout(panelLoadLayout);
        panelLoadLayout.setHorizontalGroup(
            panelLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 664, Short.MAX_VALUE)
        );
        panelLoadLayout.setVerticalGroup(
            panelLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        btnRefreshLoad.setBackground(new java.awt.Color(51, 122, 183));
        btnRefreshLoad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefreshLoad.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshLoad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        btnRefreshLoad.setText("Refresh");
        btnRefreshLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshLoadActionPerformed(evt);
            }
        });

        btnBack2.setBackground(new java.awt.Color(51, 122, 183));
        btnBack2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack2.setForeground(new java.awt.Color(255, 255, 255));
        btnBack2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack2.setText("Back");
        btnBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LoadLayout = new javax.swing.GroupLayout(Load);
        Load.setLayout(LoadLayout);
        LoadLayout.setHorizontalGroup(
            LoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelLoad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LoadLayout.createSequentialGroup()
                        .addComponent(btnBack2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefreshLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        LoadLayout.setVerticalGroup(
            LoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLoad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(LoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefreshLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Load  ", Load);

        Health.setBackground(new java.awt.Color(255, 255, 255));

        panelHealth.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Health Graph", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout panelHealthLayout = new javax.swing.GroupLayout(panelHealth);
        panelHealth.setLayout(panelHealthLayout);
        panelHealthLayout.setHorizontalGroup(
            panelHealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelHealthLayout.setVerticalGroup(
            panelHealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        btnRefreshHealth.setBackground(new java.awt.Color(51, 122, 183));
        btnRefreshHealth.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefreshHealth.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshHealth.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        btnRefreshHealth.setText("Refresh");
        btnRefreshHealth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshHealthActionPerformed(evt);
            }
        });

        btnBack1.setBackground(new java.awt.Color(51, 122, 183));
        btnBack1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack1.setForeground(new java.awt.Color(255, 255, 255));
        btnBack1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack1.setText("Back");
        btnBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout HealthLayout = new javax.swing.GroupLayout(Health);
        Health.setLayout(HealthLayout);
        HealthLayout.setHorizontalGroup(
            HealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HealthLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(HealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HealthLayout.createSequentialGroup()
                        .addComponent(btnBack1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 391, Short.MAX_VALUE)
                        .addComponent(btnRefreshHealth, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelHealth, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        HealthLayout.setVerticalGroup(
            HealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HealthLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelHealth, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HealthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBack1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRefreshHealth, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Health  ", Health);

        Report.setBackground(new java.awt.Color(255, 255, 255));

        pnlTemp.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Temperature", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout pnlTempLayout = new javax.swing.GroupLayout(pnlTemp);
        pnlTemp.setLayout(pnlTempLayout);
        pnlTempLayout.setHorizontalGroup(
            pnlTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlTempLayout.setVerticalGroup(
            pnlTempLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        pnlFrq.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Frequency", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout pnlFrqLayout = new javax.swing.GroupLayout(pnlFrq);
        pnlFrq.setLayout(pnlFrqLayout);
        pnlFrqLayout.setHorizontalGroup(
            pnlFrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlFrqLayout.setVerticalGroup(
            pnlFrqLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 187, Short.MAX_VALUE)
        );

        pnlPSD.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Power Spectral Density", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout pnlPSDLayout = new javax.swing.GroupLayout(pnlPSD);
        pnlPSD.setLayout(pnlPSDLayout);
        pnlPSDLayout.setHorizontalGroup(
            pnlPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 303, Short.MAX_VALUE)
        );
        pnlPSDLayout.setVerticalGroup(
            pnlPSDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 186, Short.MAX_VALUE)
        );

        pnlLoad.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Load", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        javax.swing.GroupLayout pnlLoadLayout = new javax.swing.GroupLayout(pnlLoad);
        pnlLoad.setLayout(pnlLoadLayout);
        pnlLoadLayout.setHorizontalGroup(
            pnlLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 303, Short.MAX_VALUE)
        );
        pnlLoadLayout.setVerticalGroup(
            pnlLoadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 186, Short.MAX_VALUE)
        );

        btnLive.setBackground(new java.awt.Color(51, 122, 183));
        btnLive.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnLive.setForeground(new java.awt.Color(255, 255, 255));
        btnLive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Play_20px.png"))); // NOI18N
        btnLive.setText("Start Live");
        btnLive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLiveActionPerformed(evt);
            }
        });

        btnStop.setBackground(new java.awt.Color(51, 122, 183));
        btnStop.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnStop.setForeground(new java.awt.Color(255, 255, 255));
        btnStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Stop_20px.png"))); // NOI18N
        btnStop.setText("Stop Live");
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(51, 122, 183));
        btnBack.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack.setForeground(new java.awt.Color(255, 255, 255));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ReportLayout = new javax.swing.GroupLayout(Report);
        Report.setLayout(ReportLayout);
        ReportLayout.setHorizontalGroup(
            ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBack)
                    .addComponent(pnlPSD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlLoad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ReportLayout.createSequentialGroup()
                        .addComponent(btnLive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStop))
                    .addComponent(pnlFrq, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        ReportLayout.setVerticalGroup(
            ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ReportLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLive, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStop, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlFrq, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlPSD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlLoad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54))
        );

        jTabbedPane1.addTab("Instant Report  ", Report);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnShowPerformanceChartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowPerformanceChartActionPerformed
        // TODO add your handling code here:
        createPerformanceChart();
    }//GEN-LAST:event_btnShowPerformanceChartActionPerformed

    private void btnShowEncounterChartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowEncounterChartActionPerformed
        // TODO add your handling code here:
        createEncounterChart();
    }//GEN-LAST:event_btnShowEncounterChartActionPerformed

    private void btnRefreshFrqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshFrqActionPerformed
        // TODO add your handling code here:
        createFrequencyChart(panelFrq, false);
    }//GEN-LAST:event_btnRefreshFrqActionPerformed

    private void btnRefreshTempActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshTempActionPerformed
        // TODO add your handling code here:
        createTemperatureChart(panelTemp, false);
    }//GEN-LAST:event_btnRefreshTempActionPerformed

    private void btnRefreshPSDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshPSDActionPerformed
        // TODO add your handling code here:
        createPSDChart(panelPSD, false);
    }//GEN-LAST:event_btnRefreshPSDActionPerformed

    private void btnRefreshLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshLoadActionPerformed
        // TODO add your handling code here:
        createLoadChart(panelLoad, false);
    }//GEN-LAST:event_btnRefreshLoadActionPerformed

    private void btnRefreshHealthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshHealthActionPerformed
        // TODO add your handling code here:
        createHealthChart();
    }//GEN-LAST:event_btnRefreshHealthActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnBack1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack1ActionPerformed

    private void btnBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack2ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack2ActionPerformed

    private void btnBack3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack3ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack3ActionPerformed

    private void btnBack4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack4ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack4ActionPerformed

    private void btnBack5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack5ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack5ActionPerformed

    private void btnBack6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack6ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack6ActionPerformed

    private void btnBack7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack7ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack7ActionPerformed

    private void btnBack8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack8ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack8ActionPerformed

    private void btnLiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLiveActionPerformed
        // TODO add your handling code here:
        timer1 = new Timer();
        timer1.schedule(new java.util.TimerTask() {

            @Override
            public void run() {
                createTemperatureChart(pnlTemp, true);
                createFrequencyChart(pnlFrq, true);
                createPSDChart(pnlPSD, true);
                createLoadChart(pnlLoad, true);
            }

        }, 1000, 2000);

    }//GEN-LAST:event_btnLiveActionPerformed

    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
        // TODO add your handling code here:
        timer1.cancel();
    }//GEN-LAST:event_btnStopActionPerformed

    private void createEncounterChart() {
        DefaultCategoryDataset encounterDataset = new DefaultCategoryDataset();
        ArrayList<Encounter> encounterList = md.getEncounterHistory().getEncounterList();
        /*At least 2 performance records needed to show chart */
//        if (encounterList.isEmpty() || encounterList.size() == 1) {
//            JOptionPane.showMessageDialog(this, "No encounter or only one encounter found. At least 2 encounter records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
//            return;
//        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (Encounter ec : encounterList) {
            int failure = 0;
            if (ec.isFailure()) {
                failure = 1;
            }
            int maint = 0;
            if (ec.isMaintenance()) {
                maint = 1;
            }
            encounterDataset.addValue(failure, "Failure", ft.format(ec.getEncounterTimestamp()));
            encounterDataset.addValue(maint, "Maintenance", ft.format(ec.getEncounterTimestamp()));
        }

        JFreeChart Chart = ChartFactory.createBarChart3D("Encounter Chart", "Date", "Values", encounterDataset, PlotOrientation.VERTICAL, true, false, false);
        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        ChartPlot.setBackgroundPaint(Color.lightGray);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        ChartFrame chartFrame = new ChartFrame("Chart", Chart);
        chartFrame.setVisible(true);
        chartFrame.setSize(900, 700);
    }

    private void createPerformanceChart() {
        DefaultCategoryDataset performanceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        int i = 0;
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (i > 0) {
                performanceDataset.addValue(pa.getTemperature(), "Temperature", ft.format(pa.getSensorTimestamp()));
                performanceDataset.addValue(pa.getFrequency(), "Frequency", ft.format(pa.getSensorTimestamp()));
                performanceDataset.addValue(pa.getPowerSpectralDensity(), "Power Spectral Density", ft.format(pa.getSensorTimestamp()));
                performanceDataset.addValue(pa.getLoad(), "Load", ft.format(pa.getSensorTimestamp()));
            }
            i++;
        }

        JFreeChart Chart = ChartFactory.createBarChart3D("Performance Chart", "Date", "Values", performanceDataset, PlotOrientation.VERTICAL, true, false, false);
        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        ChartPlot.setBackgroundPaint(Color.lightGray);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        ChartFrame chartFrame = new ChartFrame("Chart", Chart);
        chartFrame.setVisible(true);
        chartFrame.setSize(900, 700);
    }

    private void createTemperatureChart(JPanel panelTemp, boolean isLive) {
        DefaultCategoryDataset performaceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (pa.getSensorTimestamp() != null) {
                Date date = new Date();
                int curdate = (int) (date.getTime() / 1000);
                int performancetime = (int) (pa.getSensorTimestamp().getTime() / 1000);
                //if (curdate - performancetime <= 200) {
                performaceDataset.addValue(pa.getTemperature(), "Temparature", ft.format(pa.getSensorTimestamp()));
                //}
            }
        }
        JFreeChart Chart;
        if (!isLive) {
            Chart = ChartFactory.createLineChart("Temparature report", "Date", "Temparature", performaceDataset, PlotOrientation.VERTICAL, true, false, false);
        } else {
            Chart = ChartFactory.createLineChart("Temparature report", null, "Temparature", performaceDataset, PlotOrientation.VERTICAL, false, false, false);
        }
        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        if (!isLive) {
            ValueAxis yAxis = ChartPlot.getRangeAxis();
            yAxis.setRange(-25.0, 60.0);
        }
        ChartPlot.setBackgroundPaint(Color.BLACK);
        ChartPlot.setDomainGridlinePaint(Color.WHITE);
        ChartPlot.getRenderer().setSeriesPaint(0, Color.GREEN);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        if (isLive) {
            Font font = new Font("Dialog", Font.PLAIN, 10);
            DomainAxis.setTickLabelFont(font);
            DomainAxis.setTickLabelsVisible(false);
            RangeAxis.setTickLabelFont(font);
        }

        ValueMarker marker = new ValueMarker(testDate.getMinTemperature());
        marker.setLabel("Minimum");
        marker.setLabelPaint(Color.RED);
        Font font = new Font("Dialog", Font.PLAIN, 10);
        marker.setLabelFont(font);
        marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker);

        ValueMarker marker1 = new ValueMarker(testDate.getMaxTemperature());
        marker1.setLabel("Maximum");
        marker1.setLabelPaint(Color.RED);
        marker1.setLabelFont(font);
        marker1.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker1.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker1.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker1);

        ChartPanel chartPanel = new ChartPanel(Chart);
        chartPanel.setMouseWheelEnabled(true);
        if (isLive) {
            chartPanel.setPreferredSize(new Dimension(300, 200));
        }
        panelTemp.setLayout(new java.awt.BorderLayout());
        panelTemp.add(chartPanel, BorderLayout.CENTER);
        panelTemp.validate();

    }

    private void createFrequencyChart(JPanel panelFrq, boolean isLive) {
        DefaultCategoryDataset performaceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (pa.getSensorTimestamp() != null) {
                Date date = new Date();
                int curdate = (int) (date.getTime() / 1000);
                int performancetime = (int) (pa.getSensorTimestamp().getTime() / 1000);
                //if (curdate - performancetime <= 200) {
                performaceDataset.addValue(pa.getFrequency(), "Frequency", ft.format(pa.getSensorTimestamp()));
                //}
            }
        }

        JFreeChart Chart;
        if (!isLive) {
            Chart = ChartFactory.createLineChart("Frequency report", "Date", "Frequency", performaceDataset, PlotOrientation.VERTICAL, true, false, false);
        } else {
            Chart = ChartFactory.createLineChart("Frequency report", null, "Frequency", performaceDataset, PlotOrientation.VERTICAL, false, false, false);
        }

        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        if (!isLive) {
            ValueAxis yAxis = ChartPlot.getRangeAxis();
            yAxis.setRange(-10.0, 100.0);
        }
        ChartPlot.setBackgroundPaint(Color.BLACK);
        ChartPlot.setDomainGridlinePaint(Color.WHITE);
        ChartPlot.getRenderer().setSeriesPaint(0, Color.GREEN);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        if (isLive) {
            Font font = new Font("Dialog", Font.PLAIN, 10);
            DomainAxis.setTickLabelFont(font);
            DomainAxis.setTickLabelsVisible(false);
            RangeAxis.setTickLabelFont(font);
        }
        ValueMarker marker = new ValueMarker(testDate.getMinFrequency());
        marker.setLabel("Minimum");
        marker.setLabelPaint(Color.RED);
        Font font = new Font("Dialog", Font.PLAIN, 10);
        marker.setLabelFont(font);
        marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker);

        ValueMarker marker1 = new ValueMarker(testDate.getMaxFrequency());
        marker1.setLabel("Maximum");
        marker1.setLabelPaint(Color.RED);
        marker1.setLabelFont(font);
        marker1.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker1.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker1.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker1);

        ChartPanel chartPanel = new ChartPanel(Chart);
        chartPanel.setMouseWheelEnabled(true);
        if (isLive) {
            chartPanel.setPreferredSize(new Dimension(300, 200));
        }
        panelFrq.setLayout(new java.awt.BorderLayout());
        panelFrq.add(chartPanel, BorderLayout.CENTER);
        panelFrq.validate();
    }

    private void createPSDChart(JPanel panelPSD, boolean isLive) {
        DefaultCategoryDataset performaceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (pa.getSensorTimestamp() != null) {
                Date date = new Date();
                int curdate = (int) (date.getTime() / 1000);
                int performancetime = (int) (pa.getSensorTimestamp().getTime() / 1000);
                //if (curdate - performancetime <= 200) {
                performaceDataset.addValue(pa.getPowerSpectralDensity(), "Power Spectral Density", ft.format(pa.getSensorTimestamp()));
                //}
            }
        }

        JFreeChart Chart;
        if (!isLive) {
            Chart = ChartFactory.createLineChart("Power Spectral Density report", "Date", "Power Spectral Density", performaceDataset, PlotOrientation.VERTICAL, true, false, false);
        } else {
            Chart = ChartFactory.createLineChart("Power Spectral Density report", null, "Power Spectral Density", performaceDataset, PlotOrientation.VERTICAL, false, false, false);
        }

        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        if (!isLive) {
            ValueAxis yAxis = ChartPlot.getRangeAxis();
            yAxis.setRange(-10.0, 180.0);
        }
        ChartPlot.setBackgroundPaint(Color.BLACK);
        ChartPlot.setDomainGridlinePaint(Color.WHITE);
        ChartPlot.getRenderer().setSeriesPaint(0, Color.GREEN);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        if (isLive) {
            Font font = new Font("Dialog", Font.PLAIN, 10);
            DomainAxis.setTickLabelFont(font);
            DomainAxis.setTickLabelsVisible(false);
            RangeAxis.setTickLabelFont(font);
        }
        ValueMarker marker = new ValueMarker(testDate.getMinPowerSpectralDensity());
        marker.setLabel("Minimum");
        marker.setLabelPaint(Color.RED);
        Font font = new Font("Dialog", Font.PLAIN, 10);
        marker.setLabelFont(font);
        marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker);

        ValueMarker marker1 = new ValueMarker(testDate.getMaxPowerSpectralDensity());
        marker1.setLabel("Maximum");
        marker1.setLabelPaint(Color.RED);
        marker1.setLabelFont(font);
        marker1.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker1.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker1.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker1);

        ChartPanel chartPanel = new ChartPanel(Chart);
        chartPanel.setMouseWheelEnabled(true);
        if (isLive) {
            chartPanel.setPreferredSize(new Dimension(300, 180));
        }
        panelPSD.setLayout(new java.awt.BorderLayout());
        panelPSD.add(chartPanel, BorderLayout.CENTER);
        panelPSD.validate();
    }

    private void createLoadChart(JPanel panelLoad, boolean isLive) {
        DefaultCategoryDataset performaceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (pa.getSensorTimestamp() != null) {
                Date date = new Date();
                int curdate = (int) (date.getTime() / 1000);
                int performancetime = (int) (pa.getSensorTimestamp().getTime() / 1000);
                //if (curdate - performancetime <= 200) {
                performaceDataset.addValue(pa.getLoad(), "Load", ft.format(pa.getSensorTimestamp()));
                //}
            }
        }

        JFreeChart Chart;
        if (!isLive) {
            Chart = ChartFactory.createLineChart("Load report", "Date", "Load", performaceDataset, PlotOrientation.VERTICAL, true, false, false);
        } else {
            Chart = ChartFactory.createLineChart("Load report", null, "Load", performaceDataset, PlotOrientation.VERTICAL, false, false, false);
        }

        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        if (!isLive) {
            ValueAxis yAxis = ChartPlot.getRangeAxis();
            yAxis.setRange(-10.0, 180.0);
        }
        ChartPlot.setBackgroundPaint(Color.BLACK);
        ChartPlot.setDomainGridlinePaint(Color.WHITE);
        ChartPlot.getRenderer().setSeriesPaint(0, Color.GREEN);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        if (isLive) {
            Font font = new Font("Dialog", Font.PLAIN, 10);
            DomainAxis.setTickLabelFont(font);
            DomainAxis.setTickLabelsVisible(false);
            RangeAxis.setTickLabelFont(font);
        }
        ValueMarker marker = new ValueMarker(testDate.getMinLoad());
        marker.setLabel("Minimum");
        marker.setLabelPaint(Color.RED);
        Font font = new Font("Dialog", Font.PLAIN, 10);
        marker.setLabelFont(font);
        marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker);

        ValueMarker marker1 = new ValueMarker(testDate.getMaxLoad());
        marker1.setLabel("Maximum");
        marker1.setLabelPaint(Color.RED);
        marker1.setLabelFont(font);
        marker1.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker1.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker1.setPaint(Color.RED);
        ChartPlot.addRangeMarker(marker1);

        ChartPanel chartPanel = new ChartPanel(Chart);
        chartPanel.setMouseWheelEnabled(true);
        if (isLive) {
            chartPanel.setPreferredSize(new Dimension(300, 180));
        }
        panelLoad.setLayout(new java.awt.BorderLayout());
        panelLoad.add(chartPanel, BorderLayout.CENTER);
        panelLoad.validate();
    }

    private void createHealthChart() {
        DefaultCategoryDataset performaceDataset = new DefaultCategoryDataset();
        ArrayList<PerformanceAttribute> performanceList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
        /*At least 2 performance records needed to show chart */
        if (performanceList.isEmpty() || performanceList.size() == 1) {
            JOptionPane.showMessageDialog(this, "No performance or only one performance found. At least 2 performance records needed to show chart!", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss");
        for (PerformanceAttribute pa : performanceList) {
            if (pa.getSensorTimestamp() != null) {
                Date date = new Date();
                int curdate = (int) (date.getTime() / 1000);
                int performancetime = (int) (pa.getSensorTimestamp().getTime() / 1000);
                //if (curdate - performancetime <= 200) {
                performaceDataset.addValue(pa.getHealth(), "Health", ft.format(pa.getSensorTimestamp()));
                //}
            }
        }

        JFreeChart Chart = ChartFactory.createLineChart("Health report", "Date", "Health %", performaceDataset, PlotOrientation.VERTICAL, true, false, false);

        Chart.setBackgroundPaint(Color.white);
        CategoryPlot ChartPlot = Chart.getCategoryPlot();
        ValueAxis yAxis = ChartPlot.getRangeAxis();
        yAxis.setRange(-10.0, 110.0);
        ChartPlot.setBackgroundPaint(Color.BLACK);
        ChartPlot.setDomainGridlinePaint(Color.WHITE);
        ChartPlot.getRenderer().setSeriesPaint(0, Color.GREEN);

        CategoryAxis DomainAxis = ChartPlot.getDomainAxis();
        DomainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        NumberAxis RangeAxis = (NumberAxis) ChartPlot.getRangeAxis();
        RangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        ValueMarker marker = new ValueMarker(0);
        marker.setLabel("Minimum");
        marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker.setPaint(Color.WHITE);
        ChartPlot.addRangeMarker(marker);

        ValueMarker marker1 = new ValueMarker(100);
        marker1.setLabel("Maximum");
        marker1.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
        marker1.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        marker1.setPaint(Color.WHITE);
        ChartPlot.addRangeMarker(marker1);

        ChartPanel chartPanel = new ChartPanel(Chart);
        chartPanel.setMouseWheelEnabled(true);
        panelHealth.setLayout(new java.awt.BorderLayout());
        panelHealth.add(chartPanel, BorderLayout.CENTER);
        panelHealth.validate();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel DeviceEncounter;
    private javax.swing.JPanel DeviceHistory;
    private javax.swing.JPanel Frq;
    private javax.swing.JPanel Health;
    private javax.swing.JPanel Load;
    private javax.swing.JPanel PSD;
    private javax.swing.JPanel Report;
    private javax.swing.JPanel Temp;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnBack1;
    private javax.swing.JButton btnBack2;
    private javax.swing.JButton btnBack3;
    private javax.swing.JButton btnBack4;
    private javax.swing.JButton btnBack5;
    private javax.swing.JButton btnBack6;
    private javax.swing.JButton btnBack7;
    private javax.swing.JButton btnBack8;
    private javax.swing.JButton btnLive;
    private javax.swing.JButton btnRefreshFrq;
    private javax.swing.JButton btnRefreshHealth;
    private javax.swing.JButton btnRefreshLoad;
    private javax.swing.JButton btnRefreshPSD;
    private javax.swing.JButton btnRefreshTemp;
    private javax.swing.JButton btnShowEncounterChart;
    private javax.swing.JButton btnShowPerformanceChart;
    private javax.swing.JButton btnStop;
    private javax.swing.JComboBox<String> cbDeviceName;
    private com.toedter.calendar.JDateChooser jDtExpireDate;
    private com.toedter.calendar.JDateChooser jDthMnfDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel panelDGI;
    private javax.swing.JPanel panelFrq;
    private javax.swing.JPanel panelHealth;
    private javax.swing.JPanel panelLoad;
    private javax.swing.JPanel panelPSD;
    private javax.swing.JPanel panelTemp;
    private javax.swing.JPanel pnlFrq;
    private javax.swing.JPanel pnlLoad;
    private javax.swing.JPanel pnlPSD;
    private javax.swing.JPanel pnlTemp;
    private javax.swing.JTable tbEncounter;
    private javax.swing.JTable tblPerformanceHistory;
    private javax.swing.JTextField txtChargeTime;
    private javax.swing.JTextField txtDRC;
    private javax.swing.JTextField txtDeviceCode;
    private javax.swing.JTextField txtFrq;
    private javax.swing.JTextField txtLoad;
    private javax.swing.JTextField txtPSD;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JSpinner txtQuantity;
    private javax.swing.JTextField txtTemp;
    // End of variables declaration//GEN-END:variables
}
