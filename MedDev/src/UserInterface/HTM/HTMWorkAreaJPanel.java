/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HTM;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.HTMOrganization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class HTMWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form HTMWorkAreaJPanel
     */
    JPanel userProcessContainer;
    HTMOrganization htmOrganization;
    UserAccount account;
    Enterprise enterprise;
    State state;
    EcoSystem system;

    public HTMWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, HTMOrganization htmOrganization, Enterprise enterprise, State state, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.htmOrganization = htmOrganization;
        this.enterprise = enterprise;
        this.state = state;
        this.system = system;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLiveDate = new javax.swing.JButton();
        btnForecast = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Health Technical Management Work Area", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        btnLiveDate.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnLiveDate.setForeground(java.awt.Color.blue);
        btnLiveDate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Video_Call_48px_1.png"))); // NOI18N
        btnLiveDate.setText("<html><center>"+"Device"+"<br>"+"Live Data"+"</center></html>");
        btnLiveDate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLiveDate.setHideActionText(true);
        btnLiveDate.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLiveDate.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLiveDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLiveDateActionPerformed(evt);
            }
        });

        btnForecast.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnForecast.setForeground(java.awt.Color.blue);
        btnForecast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Stocks_64px.png"))); // NOI18N
        btnForecast.setText("<html><center>"+"Forecast"+"<br>"+"Maintenance"+"</center></html>");
        btnForecast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnForecast.setHideActionText(true);
        btnForecast.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnForecast.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnForecast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnForecastActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/manfadmin.jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(btnLiveDate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnForecast, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(82, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(158, 158, 158)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnForecast, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLiveDate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnLiveDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLiveDateActionPerformed
        // TODO add your handling code here:
        SelectMedicalDeviceJPanel SelectMedicalDeviceJPanel = new SelectMedicalDeviceJPanel(userProcessContainer, enterprise, state, system, htmOrganization, false);
        userProcessContainer.add("SelectMedicalDeviceJPanel", SelectMedicalDeviceJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnLiveDateActionPerformed

    private void btnForecastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForecastActionPerformed
        // TODO add your handling code here:
        SelectMedicalDeviceJPanel SelectMedicalDeviceJPanel = new SelectMedicalDeviceJPanel(userProcessContainer, enterprise, state, system, htmOrganization, true);
        userProcessContainer.add("SelectMedicalDeviceJPanel", SelectMedicalDeviceJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnForecastActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnForecast;
    private javax.swing.JButton btnLiveDate;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
