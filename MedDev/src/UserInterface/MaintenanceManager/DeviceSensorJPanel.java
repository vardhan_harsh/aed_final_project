/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.MaintenanceManager;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.MedicalDevice.Encounter;
import Business.MedicalDevice.EncounterHistory;
import Business.MedicalDevice.MedicalDevice;
import Business.MedicalDevice.PerformanceAttribute;
import Business.MedicalDevice.PerformanceAttributeHistory;
import Business.Network.State;
import Business.Organization.MaintenanceDepartmentOrganization;
import Business.UserAccount.UserAccount;
import Business.Utilities.JSwitch;
import Business.Utilities.MedicalDeviceTestData;
import Business.Utilities.Utility;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Harsh
 */
public class DeviceSensorJPanel extends javax.swing.JPanel {

    /**
     * Creates new form DeviceSensorJPanel
     */
    JPanel userProcessContainer;
    MaintenanceDepartmentOrganization departmentOrganization;
    UserAccount account;
    Enterprise enterprise;
    State state;
    EcoSystem system;
    MedicalDevice md;
    Timer timer1;
    Timer timer2;
    Timer timer3;
    Timer timer4;
    Timer timer5;
    MedicalDeviceTestData testData;
    JSwitch temp;
    JSwitch frqy;
    JSwitch chargeTime;
    JSwitch psd;
    JSwitch load;
    double depreciation = 0;
    double maintenanceThreshhold = 0;
    PerformanceAttribute lastPa;

    public DeviceSensorJPanel(JPanel userProcessContainer, UserAccount account, MaintenanceDepartmentOrganization departmentOrganization, MedicalDevice md, Enterprise enterprise, State state, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.departmentOrganization = departmentOrganization;
        this.enterprise = enterprise;
        this.state = state;
        this.system = system;
        this.md = md;
        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
        } else {
            lastPa = md.getPerformanceAttribute();
        }
        maintenanceThreshhold = lastPa.getHealth() - (lastPa.getHealth() * 30) / 100;
        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 2) / 100;
        txtDeviceName.setText(md.getDeviceName().toUpperCase());
        testData = new MedicalDeviceTestData(md.getPerformanceAttribute().getDeviceRegulatoryClass(), system.getCountryName(state));
        temp = new JSwitch("On", "Off", Color.red, Color.green, false);
        frqy = new JSwitch("On", "Off", Color.red, Color.green, false);
        //chargeTime = new JSwitch("On", "Off", Color.red, Color.green, false);
        psd = new JSwitch("On", "Off", Color.red, Color.green, false);
        load = new JSwitch("On", "Off", Color.red, Color.green, false);
        //temp.setPreferredSize(new Dimension(200, 30));
        panelSwitch.setLayout(new BoxLayout(panelSwitch, BoxLayout.Y_AXIS));
        panelSwitch.add(temp);
        panelSwitch.add(frqy);
        panelSwitch.add(psd);
        panelSwitch.add(load);
        //panelSwitch.add(chargeTime);
        temp.addActionListener((ActionEvent e) -> {
            if (temp.isSelected() && !togglebutton.isSelected()) {
                tempOn();
            } else {
                tempOff();
            }
        });
        frqy.addActionListener((ActionEvent e) -> {
            if (frqy.isSelected() && !togglebutton.isSelected()) {
                frqyOn();
            } else {
                frqyOff();
            }
        });
        psd.addActionListener((ActionEvent e) -> {
            if (psd.isSelected() && !togglebutton.isSelected()) {
                psdOn();
            } else {
                psdOff();
            }
        });
        load.addActionListener((ActionEvent e) -> {
            if (load.isSelected() && !togglebutton.isSelected()) {
                loadOn();
            } else {
                loadOff();
            }
        });

        if (!md.getEncounterHistory().getEncounterList().isEmpty() && md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == true) {
            btnClear.setEnabled(false);
        }
        refreshTable();
        timeDiff();
    }

    private void timeDiff() {
        ArrayList<Encounter> ecList = md.getEncounterHistory().getEncounterList();
        if (ecList.size() > 0) {
            if (!ecList.get(ecList.size() - 1).isMaintenance()) {
                ArrayList<PerformanceAttribute> paList = md.getPerformanceHistory().getPerformanceHistoryAttributeList();
                if (paList.size() > 1 && ecList.size() > 2) {
                    int i = 0;
                    long ts1 = 0;
                    int k = 0;
                    for (int j = ecList.size() - 1; j > 0; j--) {
                        if (!ecList.get(j).isMaintenance() && i == 2) {
                            for (PerformanceAttribute pa : paList) {
                                if (pa.getSensorTimestamp().toString().equals(ecList.get(j).getEncounterTimestamp().toString())) {
                                    ts1 = paList.get(k + 1).getSensorTimestamp().getTime();
                                    break;
                                }
                                k++;
                            }
                        }
                        if (i > 2) {
                            break;
                        }
                        i++;
                    }
                    long ts2 = paList.get(paList.size() - 2).getSensorTimestamp().getTime();
                    long tdiff = ts2 - ts1;
                    String tHrDiff = (int) TimeUnit.MILLISECONDS.toHours(tdiff) > 0 ? String.valueOf((int) TimeUnit.MILLISECONDS.toHours(tdiff)) + " hours " : "";
                    int tMinDiff = (int) TimeUnit.MILLISECONDS.toMinutes(tdiff);
                    int tSecDiff = (int) (TimeUnit.MILLISECONDS.toSeconds(tdiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(tdiff)));
                    String strMin = "";
                    if (tMinDiff > 0) {
                        strMin = String.valueOf(tMinDiff) + " minutes ";
                    }
                    lblDiff.setText(tHrDiff + strMin + String.valueOf(tSecDiff) + " seconds");
                } else {
                    panelEncounter.setVisible(false);
                }
            } else {
                panelEncounter.setVisible(false);
            }
        } else {
            panelEncounter.setVisible(false);
        }
    }

    private void tempOn() {
        timer1 = new Timer();
        timer1.schedule(new java.util.TimerTask() {

            @Override
            public void run() {
                if (md.getEncounterHistory().getEncounterList().isEmpty() || md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == false) {
                    btnClear.setEnabled(true);
                    if (depreciation > maintenanceThreshhold) {
                        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
                            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                        } else {
                            lastPa = md.getPerformanceAttribute();
                        }
                        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 0.5) / 100;
                        if (depreciation > maintenanceThreshhold) {
                            double temp = Utility.randDouble((int) testData.getMinTemperature() - 10, (int) testData.getMaxTemperature() + 10);
                            if (temp <= testData.getMaxTemperature() + 10
                                    && temp >= testData.getMinTemperature() - 10) {
                                PerformanceAttribute pa = md.getPerformanceHistory().createAndAddPerformanceAttribute();
                                //Temperature rate range

                                pa.setHealth(depreciation);
                                pa.setTemperature(temp);
                                Date date = new Date();
                                pa.setSensorTimestamp(date);
                                refreshTable();
                            } else {
                                tempOff();
                                switchOnPerformance(false);
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Temprature is out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            tempOff();
                            switchOnPerformance(false);
                            Encounter ec = md.getEncounterHistory().createAndAddEncounter();
                            Date date = new Date();
                            ec.setEncounterTimestamp(date);
                            ec.setMaintenance(true);
                            JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Require maintenance. Sent to maintenance department.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        tempOff();
                        btnClear.setEnabled(false);
                        switchOnPerformance(false);
                        JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    tempOff();
                    switchOnPerformance(false);
                    JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }, 1000, 5000);
    }

    private void tempOff() {
        if (timer1 != null) {
            timer1.cancel();
        }
    }

    private void frqyOn() {
        timer2 = new Timer();

        timer2.schedule(new java.util.TimerTask() {

            @Override
            public void run() {
                if (md.getEncounterHistory().getEncounterList().isEmpty() || md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == false) {
                    btnClear.setEnabled(true);
                    if (depreciation > maintenanceThreshhold) {
                        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
                            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                        } else {
                            lastPa = md.getPerformanceAttribute();
                        }
                        lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);;
                        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 0.5) / 100;
                        if (depreciation > maintenanceThreshhold) {
                            double frq = Utility.randDouble((int) testData.getMinFrequency() - 10, (int) testData.getMaxFrequency() + 10);
                            if (frq <= testData.getMaxFrequency() + 10
                                    && frq >= testData.getMinFrequency() - 10) {
                                PerformanceAttribute pa = md.getPerformanceHistory().createAndAddPerformanceAttribute();
                                //Frequency rate range
                                pa.setHealth(depreciation);
                                pa.setFrequency(frq);
                                Date date = new Date();
                                pa.setSensorTimestamp(date);
                                refreshTable();
                            } else {
                                frqyOff();
                                switchOnPerformance(false);
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Frequency is out of the default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            frqyOff();
                            switchOnPerformance(false);
                            Encounter ec = md.getEncounterHistory().createAndAddEncounter();
                            Date date = new Date();
                            ec.setEncounterTimestamp(date);
                            ec.setMaintenance(true);
                            JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Require maintenance. Sent to maintenance department.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        frqyOff();
                        btnClear.setEnabled(false);
                        switchOnPerformance(false);
                        JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    frqyOff();
                    switchOnPerformance(false);
                    JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }, 1000, 5000);
    }

    private void frqyOff() {
        if (timer2 != null) {
            timer2.cancel();
        }
    }

    private void psdOn() {
        timer3 = new Timer();

        timer3.schedule(new java.util.TimerTask() {

            @Override
            public void run() {
                if (md.getEncounterHistory().getEncounterList().isEmpty() || md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == false) {
                    btnClear.setEnabled(true);
                    if (depreciation > maintenanceThreshhold) {
                        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
                            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                        } else {
                            lastPa = md.getPerformanceAttribute();
                        }
                        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 0.5) / 100;
                        if (depreciation > maintenanceThreshhold) {
                            double psd = Utility.randDouble((int) testData.getMinPowerSpectralDensity() - 20, (int) testData.getMaxPowerSpectralDensity() + 20);
                            if (psd <= testData.getMaxPowerSpectralDensity() + 20
                                    && psd >= testData.getMinPowerSpectralDensity() - 20) {
                                PerformanceAttribute pa = md.getPerformanceHistory().createAndAddPerformanceAttribute();
                                //PowerSpectralDensity rate range
                                pa.setHealth(depreciation);
                                pa.setPowerSpectralDensity(psd);
                                Date date = new Date();
                                pa.setSensorTimestamp(date);
                                refreshTable();
                            } else {
                                psdOff();
                                switchOnPerformance(false);
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device PowerSpectralDensity out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            psdOff();
                            switchOnPerformance(false);
                            Encounter ec = md.getEncounterHistory().createAndAddEncounter();
                            Date date = new Date();
                            ec.setEncounterTimestamp(date);
                            ec.setMaintenance(true);
                            JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Require maintenance. Sent to maintenance department.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        psdOff();
                        btnClear.setEnabled(false);
                        switchOnPerformance(false);
                        JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    psdOff();
                    switchOnPerformance(false);
                    JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }, 1000, 5000);
    }

    private void psdOff() {
        if (timer3 != null) {
            timer3.cancel();
        }
    }

    private void loadOn() {
        timer4 = new Timer();

        timer4.schedule(new java.util.TimerTask() {

            @Override
            public void run() {
                if (md.getEncounterHistory().getEncounterList().isEmpty() || md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == false) {
                    btnClear.setEnabled(true);
                    if (depreciation > maintenanceThreshhold) {
                        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
                            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                        } else {
                            lastPa = md.getPerformanceAttribute();
                        }
                        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 0.5) / 100;
                        if (depreciation > maintenanceThreshhold) {
                            int load = Utility.randInt(testData.getMinLoad(), testData.getMaxLoad() + 20);
                            if (load <= testData.getMaxLoad() + 20
                                    && load >= testData.getMinLoad()) {
                                PerformanceAttribute pa = md.getPerformanceHistory().createAndAddPerformanceAttribute();
                                //PowerSpectralDensity rate range
                                pa.setHealth(depreciation);
                                pa.setLoad(load);
                                Date date = new Date();
                                pa.setSensorTimestamp(date);
                                refreshTable();
                            } else {
                                loadOff();
                                switchOnPerformance(false);
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Load is out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            loadOff();
                            switchOnPerformance(false);
                            Encounter ec = md.getEncounterHistory().createAndAddEncounter();
                            Date date = new Date();
                            ec.setEncounterTimestamp(date);
                            ec.setMaintenance(true);
                            JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Require maintenance. Sent to maintenance department.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        loadOff();
                        btnClear.setEnabled(false);
                        switchOnPerformance(false);
                        JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    loadOff();
                    switchOnPerformance(false);
                    JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }, 1000, 5000);
    }

    private void loadOff() {
        if (timer4 != null) {
            timer4.cancel();
        }
    }

    private void refreshTable() {
        int rowCount = tblDevice.getRowCount();

        DefaultTableModel model = (DefaultTableModel) tblDevice.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (PerformanceAttribute pa : md.getPerformanceHistory().getPerformanceHistoryAttributeList()) {
            Object row[] = new Object[6];
            row[0] = pa.getTemperature();
            row[1] = pa.getFrequency();
            row[2] = pa.getPowerSpectralDensity();
            //row[3] = pa.getChargeTime();
            row[3] = pa.getLoad();
            row[4] = pa.getHealth();
            row[5] = pa.getSensorTimestamp();
            model.addRow(row);
            txtrecord.setText(String.valueOf(tblDevice.getRowCount()));
        }
    }

    private void switchOnPerformance(boolean flag) {
        temp.setSelected(flag);
        frqy.setSelected(flag);
        psd.setSelected(flag);
        load.setSelected(flag);
        //chargeTime.setSelected(flag);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblDevice = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panelSwitch = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtrecord = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        togglebutton = new javax.swing.JToggleButton();
        btnClear = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtDeviceName = new javax.swing.JTextField();
        btnBack7 = new javax.swing.JButton();
        panelEncounter = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblDiff = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device Sensor Panel", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        tblDevice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Temprature", "Frequency", "Power Density", "Load", "Health", "Date"
            }
        ));
        jScrollPane1.setViewportView(tblDevice);
        if (tblDevice.getColumnModel().getColumnCount() > 0) {
            tblDevice.getColumnModel().getColumn(5).setMinWidth(200);
            tblDevice.getColumnModel().getColumn(5).setPreferredWidth(200);
        }

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Temprature");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Frequency");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Power Spectral Density");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Load");

        panelSwitch.setBackground(new java.awt.Color(255, 255, 255));
        panelSwitch.setLayout(new javax.swing.BoxLayout(panelSwitch, javax.swing.BoxLayout.LINE_AXIS));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Records:");

        txtrecord.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtrecord.setForeground(new java.awt.Color(0, 102, 102));
        txtrecord.setEnabled(false);

        jButton1.setBackground(new java.awt.Color(51, 122, 183));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Refresh_20px.png"))); // NOI18N
        jButton1.setText("Refresh");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        togglebutton.setBackground(new java.awt.Color(51, 122, 183));
        togglebutton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        togglebutton.setForeground(new java.awt.Color(255, 255, 255));
        togglebutton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Proximity_Sensor_20px_1.png"))); // NOI18N
        togglebutton.setText("Turn On All Sensors");
        togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                togglebuttonActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(51, 122, 183));
        btnClear.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Clear_Search_20px.png"))); // NOI18N
        btnClear.setText("Clear Device Performace");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel1.setText("Device Name");

        txtDeviceName.setEditable(false);

        btnBack7.setBackground(new java.awt.Color(51, 122, 183));
        btnBack7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack7.setForeground(new java.awt.Color(255, 255, 255));
        btnBack7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack7.setText("Back");
        btnBack7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack7ActionPerformed(evt);
            }
        });

        panelEncounter.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel2.setText("Time difference for last two encounter");

        lblDiff.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDiff.setText("jLabel3");

        javax.swing.GroupLayout panelEncounterLayout = new javax.swing.GroupLayout(panelEncounter);
        panelEncounter.setLayout(panelEncounterLayout);
        panelEncounterLayout.setHorizontalGroup(
            panelEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEncounterLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDiff, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelEncounterLayout.setVerticalGroup(
            panelEncounterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEncounterLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(lblDiff)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6))
                                .addGap(18, 18, 18)
                                .addComponent(panelSwitch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnBack7)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(togglebutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelEncounter, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(66, 66, 66))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                                .addComponent(btnClear)
                                .addGap(108, 108, 108)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtrecord, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrecord, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(panelSwitch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(52, 52, 52)
                        .addComponent(btnBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(togglebutton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelEncounter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        refreshTable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void togglebuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_togglebuttonActionPerformed
        // TODO add your handling code here:
        switchOnPerformance(togglebutton.isSelected());
        if (togglebutton.isSelected()) {

            togglebutton.setText("On");
            togglebutton.setForeground(Color.GREEN);
            timer5 = new Timer();
            timer5.schedule(new java.util.TimerTask() {
                @Override
                public void run() {
                    if (md.getEncounterHistory().getEncounterList().isEmpty() || md.getEncounterHistory().getEncounterList().get(md.getEncounterHistory().getEncounterList().size() - 1).isMaintenance() == false) {
                        btnClear.setEnabled(true);
                        if (md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() > 0) {
                            lastPa = md.getPerformanceHistory().getPerformanceHistoryAttributeList().get(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                        } else {
                            lastPa = md.getPerformanceAttribute();
                        }

                        depreciation = lastPa.getHealth() - (lastPa.getHealth() * 2) / 100;
                        if (depreciation > maintenanceThreshhold) {
                            PerformanceAttribute pa = md.getPerformanceHistory().createAndAddPerformanceAttribute();
                            double temp = Utility.randDouble((int) testData.getMinTemperature() - 10, (int) testData.getMaxTemperature() + 10);
                            if (temp <= testData.getMaxTemperature() + 10
                                    && temp >= testData.getMinTemperature() - 10) {
                                //Temperature rate range
                                pa.setTemperature(temp);
                            } else {
                                md.getPerformanceHistory().getPerformanceHistoryAttributeList().remove(md.getPerformanceHistory().getPerformanceHistoryAttributeList().size() - 1);
                                timer5.cancel();
                                togglebutton.setSelected(false);
                                togglebutton.setText("Off");
                                togglebutton.setForeground(Color.BLACK);
                                switchOnPerformance(togglebutton.isSelected());
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Temprature is out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            double frq = Utility.randDouble((int) testData.getMinFrequency() - 20, (int) testData.getMaxFrequency() + 20);
                            if (frq <= testData.getMaxFrequency() + 20
                                    && frq >= testData.getMinFrequency() - 20) {
                                //Frequency rate range
                                pa.setFrequency(frq);
                            } else {
                                timer5.cancel();
                                togglebutton.setSelected(false);
                                togglebutton.setText("Off");
                                togglebutton.setForeground(Color.BLACK);
                                switchOnPerformance(togglebutton.isSelected());
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Frequency is out of the default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            double psd = Utility.randDouble((int) testData.getMinPowerSpectralDensity() - 20, (int) testData.getMaxPowerSpectralDensity() + 20);
                            if (psd <= testData.getMaxPowerSpectralDensity() + 20
                                    && psd >= testData.getMinPowerSpectralDensity() - 20) {
                                //PowerSpectralDensity rate range
                                pa.setPowerSpectralDensity(psd);
                            } else {
                                timer5.cancel();
                                togglebutton.setSelected(false);
                                togglebutton.setText("Off");
                                togglebutton.setForeground(Color.BLACK);
                                switchOnPerformance(togglebutton.isSelected());
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device PowerSpectralDensity out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            int load = Utility.randInt(testData.getMinLoad(), testData.getMaxLoad() + 20);
                            if (load <= testData.getMaxLoad() + 20
                                    && load >= testData.getMinLoad()) {
                                //PowerSpectralDensity rate range
                                pa.setLoad(load);
                            } else {
                                timer5.cancel();
                                togglebutton.setSelected(false);
                                togglebutton.setText("Off");
                                togglebutton.setForeground(Color.BLACK);
                                switchOnPerformance(togglebutton.isSelected());
                                JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Load is out of default range value.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            Date date = new Date();
                            pa.setHealth(depreciation);
                            pa.setSensorTimestamp(date);
                            refreshTable();
                        } else {
                            btnClear.setEnabled(false);
                            timer5.cancel();
                            togglebutton.setSelected(false);
                            togglebutton.setText("Off");
                            togglebutton.setForeground(Color.BLACK);
                            switchOnPerformance(togglebutton.isSelected());
                            Encounter ec = md.getEncounterHistory().createAndAddEncounter();
                            Date date = new Date();
                            ec.setEncounterTimestamp(date);
                            ec.setMaintenance(true);
                            JOptionPane.showMessageDialog(null, md.getDeviceCode() + " Device Require maintenance. Sent to maintenance department.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        timer5.cancel();
                        togglebutton.setSelected(false);
                        togglebutton.setText("Off");
                        togglebutton.setForeground(Color.BLACK);
                        switchOnPerformance(togglebutton.isSelected());
                        JOptionPane.showMessageDialog(null, "Device under maintenance.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            },
                    1000, 5000);

        } else if (!togglebutton.isSelected()) {
            timer5.cancel();
            togglebutton.setText("Off");
            togglebutton.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_togglebuttonActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        int response = JOptionPane.showConfirmDialog(null, "Do you want to continue?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.NO_OPTION) {
            JOptionPane.showMessageDialog(null, "Request cancelled!");
        } else if (response == JOptionPane.YES_OPTION) {
            PerformanceAttributeHistory ph = md.getPerformanceHistory();
            for (Iterator<PerformanceAttribute> it = ph.getPerformanceHistoryAttributeList().iterator(); it.hasNext();) {
                PerformanceAttribute vs = it.next();
                it.remove();
            }
            EncounterHistory eh = md.getEncounterHistory();
            for (Iterator<Encounter> et = eh.getEncounterList().iterator(); et.hasNext();) {
                Encounter e = et.next();
                et.remove();
            }
            refreshTable();
            txtrecord.setText("");
        }
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnBack7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack7ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack7ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack7;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDiff;
    private javax.swing.JPanel panelEncounter;
    private javax.swing.JPanel panelSwitch;
    private javax.swing.JTable tblDevice;
    private javax.swing.JToggleButton togglebutton;
    private javax.swing.JTextField txtDeviceName;
    private javax.swing.JTextField txtrecord;
    // End of variables declaration//GEN-END:variables
}
