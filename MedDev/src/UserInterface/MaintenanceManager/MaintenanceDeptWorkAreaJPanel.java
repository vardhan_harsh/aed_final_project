/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.MaintenanceManager;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.State;
import Business.Organization.MaintenanceDepartmentOrganization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class MaintenanceDeptWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form MaintenanceDeptWorkAreaJPanel
     */
    JPanel userProcessContainer;
    MaintenanceDepartmentOrganization departmentOrganization;
    UserAccount account;
    Enterprise enterprise;
    State state;
    EcoSystem system;
    
    public MaintenanceDeptWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, MaintenanceDepartmentOrganization departmentOrganization, Enterprise enterprise, State state, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.departmentOrganization = departmentOrganization;
        this.enterprise = enterprise;
        this.state = state;
        this.system = system;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRequest = new javax.swing.JButton();
        btnRequest1 = new javax.swing.JButton();
        btnManageRequest = new javax.swing.JButton();
        btnManageRequest1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Maintenance Department Work Area", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        btnRequest.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRequest.setForeground(java.awt.Color.blue);
        btnRequest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Request_Service_50px.png"))); // NOI18N
        btnRequest.setText("<html><center>"+"Request Hospital"+"<br>"+"Inventory"+"</center></html>");
        btnRequest.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRequest.setHideActionText(true);
        btnRequest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRequest.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRequestActionPerformed(evt);
            }
        });

        btnRequest1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRequest1.setForeground(java.awt.Color.blue);
        btnRequest1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Proximity_Sensor_52px.png"))); // NOI18N
        btnRequest1.setText("Sensor Status");
        btnRequest1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRequest1.setHideActionText(true);
        btnRequest1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRequest1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRequest1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRequest1ActionPerformed(evt);
            }
        });

        btnManageRequest.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnManageRequest.setForeground(java.awt.Color.blue);
        btnManageRequest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Welder_50px.png"))); // NOI18N
        btnManageRequest.setText("<html><center>"+"Request"+"<br>"+"Manufacturer List"+"</center></html>");
        btnManageRequest.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnManageRequest.setHideActionText(true);
        btnManageRequest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnManageRequest.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnManageRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageRequestActionPerformed(evt);
            }
        });

        btnManageRequest1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnManageRequest1.setForeground(java.awt.Color.blue);
        btnManageRequest1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Printer_Maintenance_48px_1.png"))); // NOI18N
        btnManageRequest1.setText("<html><center>"+"Device"+"<br>"+"Maintenance"+"</center></html>");
        btnManageRequest1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnManageRequest1.setHideActionText(true);
        btnManageRequest1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnManageRequest1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnManageRequest1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageRequest1ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/maintenancedepartment.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnManageRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnRequest1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnManageRequest1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(53, 53, 53))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(122, 122, 122)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnManageRequest1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRequest1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnManageRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRequestActionPerformed
        // TODO add your handling code here:
        RequestDeviceJPanel RequestDeviceStatusJPanel = new RequestDeviceJPanel(userProcessContainer, account, departmentOrganization, enterprise, state);
        userProcessContainer.add("RequestDeviceStatusJPanel", RequestDeviceStatusJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnRequestActionPerformed

    private void btnRequest1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRequest1ActionPerformed
        // TODO add your handling code here:
        SelectMedicalDeviceJPanel SelectMedicalDeviceJPanel = new SelectMedicalDeviceJPanel(userProcessContainer, account, departmentOrganization, enterprise, state, system);
        userProcessContainer.add("SelectMedicalDeviceJPanel", SelectMedicalDeviceJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnRequest1ActionPerformed

    private void btnManageRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageRequestActionPerformed
        // TODO add your handling code here:
        RequestDeviceFromManufactureListJPanel RequestDeviceFromManufactureListJPanel = new RequestDeviceFromManufactureListJPanel(userProcessContainer, account, departmentOrganization, enterprise, state);
        userProcessContainer.add("RequestDeviceFromManufactureListJPanel", RequestDeviceFromManufactureListJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageRequestActionPerformed

    private void btnManageRequest1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageRequest1ActionPerformed
        // TODO add your handling code here:
        DeviceUnderMaintenanceJPanel DeviceUnderMaintenanceJPanel = new DeviceUnderMaintenanceJPanel(userProcessContainer, account, departmentOrganization, enterprise, state, system);
        userProcessContainer.add("DeviceUnderMaintenanceJPanel", DeviceUnderMaintenanceJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageRequest1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageRequest;
    private javax.swing.JButton btnManageRequest1;
    private javax.swing.JButton btnRequest;
    private javax.swing.JButton btnRequest1;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
