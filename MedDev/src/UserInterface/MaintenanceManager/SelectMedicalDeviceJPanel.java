/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.MaintenanceManager;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.MedicalDevice.MedicalDevice;
import Business.Network.State;
import Business.Organization.MaintenanceDepartmentOrganization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Harsh
 */
public class SelectMedicalDeviceJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SelectMedicalDeviceJPanel
     */
    JPanel userProcessContainer;
    MaintenanceDepartmentOrganization departmentOrganization;
    UserAccount account;
    Enterprise enterprise;
    State state;
    EcoSystem system;

    public SelectMedicalDeviceJPanel(JPanel userProcessContainer, UserAccount account, MaintenanceDepartmentOrganization departmentOrganization, Enterprise enterprise, State state, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.account = account;
        this.departmentOrganization = departmentOrganization;
        this.enterprise = enterprise;
        this.state = state;
        this.system = system;
        cbDeviceName.removeAllItems();
        popuplateDeviceName();
        lblDeviceCode.setText("");
        lblDeviceName.setText("");
        lblRfId.setText("");
        lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Full_Image_100px.png")));
    }

    private void popuplateDeviceName() {
        cbDeviceName.removeAllItems();
        cbDeviceName.addItem("--Select--");
        for (MedicalDevice md : departmentOrganization.getDeviceCatalog().getMedicalDeviceList()) {
            cbDeviceName.addItem(md);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbDeviceName = new javax.swing.JComboBox();
        btnBack7 = new javax.swing.JButton();
        btnSelectDevice = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblDeviceImage = new javax.swing.JLabel();
        lblDeviceName = new javax.swing.JLabel();
        lblRfId = new javax.swing.JLabel();
        lblDeviceCode = new javax.swing.JLabel();
        lblQRCode = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Select Device", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Device Name");

        cbDeviceName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbDeviceName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbDeviceNameItemStateChanged(evt);
            }
        });

        btnBack7.setBackground(new java.awt.Color(51, 122, 183));
        btnBack7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBack7.setForeground(new java.awt.Color(255, 255, 255));
        btnBack7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Back_To_20px_2.png"))); // NOI18N
        btnBack7.setText("Back");
        btnBack7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBack7ActionPerformed(evt);
            }
        });

        btnSelectDevice.setBackground(new java.awt.Color(51, 122, 183));
        btnSelectDevice.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSelectDevice.setForeground(new java.awt.Color(255, 255, 255));
        btnSelectDevice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Select_20px.png"))); // NOI18N
        btnSelectDevice.setText("Select Device ");
        btnSelectDevice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectDeviceActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        lblDeviceImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDeviceImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblDeviceName.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDeviceName.setText("jLabel2");

        lblRfId.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblRfId.setText("jLabel2");

        lblDeviceCode.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDeviceCode.setText("jLabel2");

        lblQRCode.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDeviceImage, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDeviceName)
                    .addComponent(lblRfId)
                    .addComponent(lblDeviceCode))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblQRCode, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDeviceImage, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblRfId)
                        .addGap(18, 18, 18)
                        .addComponent(lblDeviceCode)
                        .addGap(18, 18, 18)
                        .addComponent(lblDeviceName)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQRCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBack7)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 193, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cbDeviceName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSelectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(175, 175, 175))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(169, 169, 169)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbDeviceName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSelectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 185, Short.MAX_VALUE)
                        .addComponent(btnBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBack7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBack7ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBack7ActionPerformed

    private void btnSelectDeviceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectDeviceActionPerformed
        if (cbDeviceName.getSelectedIndex() > 0) {
            MedicalDevice md = (MedicalDevice) cbDeviceName.getSelectedItem();
            DeviceSensorJPanel DepartmentDeviceSensorJPanel = new DeviceSensorJPanel(userProcessContainer, account, departmentOrganization, md, enterprise, state, system);
            userProcessContainer.add("DepartmentDeviceSensorJPanel", DepartmentDeviceSensorJPanel);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        } else {
            JOptionPane.showMessageDialog(this, "Please select device.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnSelectDeviceActionPerformed

    private void cbDeviceNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbDeviceNameItemStateChanged
        // TODO add your handling code here:
        if (cbDeviceName.getSelectedItem() instanceof MedicalDevice) {
            MedicalDevice md = (MedicalDevice) cbDeviceName.getSelectedItem();
            if (md.getDeviceName().equalsIgnoreCase("X-Ray")) {
                lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/digital-x-ray-machine-250x250.jpg")));
            } else if (md.getDeviceName().equalsIgnoreCase("MRI")) {
                lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/MRI.jpg")));
            } else if (md.getDeviceName().equalsIgnoreCase("Defibrillator")) {
                lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/defibrillator.png")));
            } else if (md.getDeviceName().equalsIgnoreCase("Ultrasound Machine")) {
                lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cardiology-ultrasound-machine-2.jpg")));
            }
            lblDeviceCode.setText(md.getDeviceCode());
            lblDeviceName.setText(md.getDeviceName());
            lblRfId.setText(String.valueOf(md.getRfid()));
            lblQRCode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/" + md.getDeviceCode() + "-QR-Code.jpg")));

        } else {
            lblDeviceCode.setText("");
            lblDeviceName.setText("");
            lblRfId.setText("");
            lblDeviceImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Full_Image_100px.png")));
            lblQRCode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons8_Full_Image_100px.png")));
        }
    }//GEN-LAST:event_cbDeviceNameItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack7;
    private javax.swing.JButton btnSelectDevice;
    private javax.swing.JComboBox cbDeviceName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblDeviceCode;
    private javax.swing.JLabel lblDeviceImage;
    private javax.swing.JLabel lblDeviceName;
    private javax.swing.JLabel lblQRCode;
    private javax.swing.JLabel lblRfId;
    // End of variables declaration//GEN-END:variables
}
